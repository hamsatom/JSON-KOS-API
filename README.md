[![pipeline status](https://gitlab.fel.cvut.cz/hamsatom/JSON-KOS-API/badges/master/pipeline.svg)](https://gitlab.fel.cvut.cz/hamsatom/JSON-KOS-API/commits/master) [![coverage report](https://gitlab.fel.cvut.cz/hamsatom/JSON-KOS-API/badges/master/coverage.svg)](https://gitlab.fel.cvut.cz/hamsatom/JSON-KOS-API/commits/master)
# JSON KOS API
**Autor:** Tomáš Hamsa  
**Dokumentace API:** https://json-kos-api.193b.starter-ca-central-1.openshiftapps.com/swagger-ui.html  
**Adresa API:** https://json-kos-api.193b.starter-ca-central-1.openshiftapps.com  
**Nasaditelný WAR archiv:** https://gitlab.fel.cvut.cz/hamsatom/JSON-KOS-API/-/jobs/artifacts/master/raw/target/ROOT.war?job=build-job  
**Javadoc:** http://hamsatom.pages.fel.cvut.cz/JSON-KOS-API
## Struktura
Aplikace zachovává striktní třívrstvou architekturu. Aplikace používá pro zdroj dat KOS API, což je webová HTTP REST služba. Data vystavujeme přes HTTP REST rozhraní pomocí Spring REST controllerů.
![Class diagram](classDiagram.png)
## Lokální instalace
Potřebný software:
-   Git (ozkoušeno na verzi 2.19.2)
-   JDK 1.8 (ozkoušeno na verzi 1.8.181)
-   Maven 3 (ozkoušeno na verzi 3.5.4)
-   Apache Tomcat (ozkoušeno na verzi 9.0.10)
-   IntelliJ IDEA Ultimate (ozkoušeno na verzi 2018.3)

V návodu budeme předpokládat, že je správně nastavena systémová proměnná
JAVA_HOME, takže jde používat Maven z termínálu a že jde používat Git z termínálu. 
Pro první část instalace otevřeme terminál a napíšeme následující
příkazy:
1.  git clone <https://gitlab.fel.cvut.cz/hamsatom/JSON-KOS-API.git>
    json-kos-api - naklonuje repozitář s projektem do složky json-kos-api
2.  cd json-kos-api
3.  mvn clean package - stáhne potřebné Maven dependence a zkompiluje
    Javu

Teď už potřebujeme jenom nasadit backend aplikace na defaultní adresu
**<http://localhost:8081>**.  
Nejjednodušší způsob jak toho docílit je naimportovat projekt do
IntelliJ IDEA Ultimate a vytvořit novou spouštěcí konfiguraci. Nová
spouštěcí konfigurace bude Tomcat server Local. Nastavíme aplikační
server do složky staženého Apache Tomcatu. Port aplikace nastavíme na
8081 a deployované artefakty na json-kos-api:war exploded.