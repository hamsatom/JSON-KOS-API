package impl.parallel;


import impl.shared.Permission;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
@ApiModel(description = "Parallel of a course")
public class Parallel implements Serializable {

  private static final long serialVersionUID = 8031428947491092366L;

  @ApiModelProperty(value = "Type of the parallel", required = true)
  private ParallelType parallelType;
  @ApiModelProperty("List of all teachers that teach the parallel")
  private List<String> teacher;
  @ApiModelProperty(value = "List of all times the parallel is taught at", required = true)
  private List<TimetableSlot> timetableSlot;
  @ApiModelProperty(value = "Maximum amount of students allowed in the parallel", required = true)
  private short capacity;
  @ApiModelProperty(value = "Amount of students in the parallel", required = true)
  private short occupied;
  @ApiModelProperty(value = "Whenever is allowed to have more students in the parallel than the capacity", required = true)
  private Permission capacityOverfill;

  public Parallel() {
    //serialization
  }

  public Parallel(ParallelType parallelType, List<String> teacher,
      List<TimetableSlot> timetableSlot, short capacity, short occupied,
      Permission capacityOverfill) {
    this.parallelType = parallelType;
    this.teacher = teacher;
    this.timetableSlot = timetableSlot;
    this.capacity = capacity;
    this.occupied = occupied;
    this.capacityOverfill = capacityOverfill;
  }

  public ParallelType getParallelType() {
    return parallelType;
  }

  public Parallel setParallelType(ParallelType parallelType) {
    this.parallelType = parallelType;
    return this;
  }

  public List<String> getTeacher() {
    return teacher;
  }

  public Parallel setTeacher(List<String> teacher) {
    this.teacher = teacher;
    return this;
  }

  public List<TimetableSlot> getTimetableSlot() {
    return timetableSlot;
  }

  public Parallel setTimetableSlot(List<TimetableSlot> timetableSlot) {
    this.timetableSlot = timetableSlot;
    return this;
  }

  public short getCapacity() {
    return capacity;
  }

  public Parallel setCapacity(short capacity) {
    this.capacity = capacity;
    return this;
  }

  public short getOccupied() {
    return occupied;
  }

  public Parallel setOccupied(short occupied) {
    this.occupied = occupied;
    return this;
  }

  public Permission getCapacityOverfill() {
    return capacityOverfill;
  }

  public Parallel setCapacityOverfill(Permission capacityOverfill) {
    this.capacityOverfill = capacityOverfill;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Parallel)) {
      return false;
    }
    Parallel parallel = (Parallel) o;
    return capacity == parallel.capacity &&
        occupied == parallel.occupied &&
        parallelType == parallel.parallelType &&
        Objects.equals(teacher, parallel.teacher) &&
        Objects.equals(timetableSlot, parallel.timetableSlot) &&
        capacityOverfill == parallel.capacityOverfill;
  }

  @Override
  public int hashCode() {
    return Objects.hash(parallelType, teacher, timetableSlot, capacity, occupied, capacityOverfill);
  }
}
