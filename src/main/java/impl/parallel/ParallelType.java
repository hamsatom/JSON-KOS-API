package impl.parallel;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
public enum ParallelType {
  LABORATORY("laboratoře"), LECTURE("přednášky"), TUTORIAL("cvičení"), UNDEFINED("nedefinovaný");

  private final String value;

  ParallelType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
