package impl.parallel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
@ApiModel(description = "One occurrence of a parallel")
public class TimetableSlot implements Serializable {

  private static final long serialVersionUID = 1961048687126642441L;

  @ApiModelProperty(value = "Number of day, starting from 1, the slot is taught on", required = true)
  private int day;
  @ApiModelProperty(value = "Amount of school hour the slot lasts for", required = true)
  private int duration;
  @ApiModelProperty(value = "Number of the first hour of the slot", required = true)
  private int firstHour;
  @ApiModelProperty(value = "Code of a room the slot is taught in", required = true)
  private String room;

  public TimetableSlot() {
    //serialization
  }

  public TimetableSlot(int day, int duration, int firstHour, String room) {
    this.day = day;
    this.duration = duration;
    this.firstHour = firstHour;
    this.room = room;
  }

  public int getDay() {
    return day;
  }

  public TimetableSlot setDay(int day) {
    this.day = day;
    return this;
  }

  public int getDuration() {
    return duration;
  }

  public TimetableSlot setDuration(int duration) {
    this.duration = duration;
    return this;
  }

  public int getFirstHour() {
    return firstHour;
  }

  public TimetableSlot setFirstHour(int firstHour) {
    this.firstHour = firstHour;
    return this;
  }

  public String getRoom() {
    return room;
  }

  public TimetableSlot setRoom(String room) {
    this.room = room;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof TimetableSlot)) {
      return false;
    }
    TimetableSlot that = (TimetableSlot) o;
    return day == that.day &&
        duration == that.duration &&
        firstHour == that.firstHour &&
        Objects.equals(room, that.room);
  }

  @Override
  public int hashCode() {
    return Objects.hash(day, duration, firstHour, room);
  }
}
