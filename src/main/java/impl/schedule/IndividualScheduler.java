package impl.schedule;

import impl.parallel.ParallelType;
import impl.subject.Subject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import org.apache.commons.lang3.tuple.ImmutablePair;
import reactor.core.publisher.Flux;

/**
 * @author Tomáš Hamsa on 11.07.2017.
 */
class IndividualScheduler {

  private static final short MAXIMUM_SCHEDULE_HOURS = 85;

  private final short theoryLessonPriority;
  private final short practicalLecturePriority;

  private final short earliestOKHour;
  private final short latestOKHour;
  private final double badHourPenalty;

  private final double freeDaysMultiplier;
  private final double hoursMultiplier;

  private final boolean separateTheoryLessons;

  IndividualScheduler(short theoryLessonPriority, short practicalLecturePriority,
      short earliestOKHour, short latestOKHour, double badHourPenalty, double freeDaysMultiplier,
      double hoursMultiplier, boolean separateTheoryLessons) {

    this.theoryLessonPriority = theoryLessonPriority;
    this.practicalLecturePriority = practicalLecturePriority;
    this.earliestOKHour = earliestOKHour;
    this.latestOKHour = latestOKHour;
    this.badHourPenalty = badHourPenalty;
    this.freeDaysMultiplier = Math.pow(10, freeDaysMultiplier);
    this.hoursMultiplier = Math.pow(10, hoursMultiplier);
    this.separateTheoryLessons = separateTheoryLessons;
  }

  private static List<? extends Subject> filterEmptySubject(
      @Nonnull Collection<? extends Subject> subjects) {
    return subjects.stream().
        filter(subject -> !subject.getParallels().isEmpty())
        .collect(Collectors.toList());
  }

  private static List<Subject> filterTheoryLessons(
      @Nonnull Collection<? extends Subject> subjects) {
    return subjects.stream()
        .filter(subject -> subject.getParallels()
            .stream()
            .anyMatch(aClass -> aClass.getParallelType() != ParallelType.LECTURE))
        .collect(Collectors.toList());
  }

  private IndividualSchedule createSchedule(@Nonnull Iterable<? extends Subject> subjects) {
    IndividualSchedule schedule = new IndividualSchedule();

    subjects.forEach(subject -> subject.getParallels()
        .stream()
        .filter(parallel -> parallel.getParallelType() == ParallelType.LECTURE)
        .forEach(parallel -> parallel.getTimetableSlot()
            .stream()
            .map(slot -> new ScheduleEntry(theoryLessonPriority, (short) slot.getDay(),
                (short) slot.getFirstHour(), (short) slot.getDuration(), slot.getRoom(),
                parallel.getTeacher(), parallel.getParallelType(), subject.getName(),
                subject.getCode()))
            .forEach(schedule::addClass)
        ));

    return schedule;
  }

  @Nonnull
  Flux<IndividualSchedule> findBestSchedule(@Nonnull Collection<? extends Subject> subjects) {
    List<? extends Subject> subjectsWithClasses = filterEmptySubject(subjects);
    if (subjectsWithClasses.isEmpty()) {
      return Flux.empty();
    }

    IndividualSchedule schedule = createSchedule(subjectsWithClasses);

    List<Subject> subjectWithLectures = filterTheoryLessons(subjectsWithClasses);
    if (subjectWithLectures.isEmpty()) {
      return Flux.just(schedule);
    }

    return addPracticalLectures(subjectWithLectures, schedule);
  }

  @Nonnull
  private Flux<IndividualSchedule> addPracticalLectures(@Nonnull List<Subject> subjects,
      @Nonnull IndividualSchedule initialSchedule) {

    Queue<ImmutablePair<IndividualSchedule, Integer>> queue = new PriorityQueue<>(
        1000, (a, b) -> Double.compare(evaluateSchedule(b.left), evaluateSchedule(a.left)));
    queue.add(new ImmutablePair<>(initialSchedule, 0));
    List<IndividualSchedule> results = new ArrayList<>(10);

    while (!queue.isEmpty()) {
      ImmutablePair<IndividualSchedule, Integer> positionPair = queue.remove();
      IndividualSchedule currentSchedule = positionPair.left;
      int position = positionPair.right;

      if (position == subjects.size()) {
        if (results.isEmpty() || !results.contains(currentSchedule)
            && evaluateSchedule(results.get(0)) <= evaluateSchedule(currentSchedule)) {
          results.add(currentSchedule);
          continue;
        }
        return Flux.fromIterable(results);
      }

      int nextPosition = position + 1;
      Subject subject = subjects.get(position);
      addToSchedule(subject, currentSchedule)
          .map(schedule -> new ImmutablePair<>(schedule, nextPosition))
          .filter(schedule -> !queue.contains(schedule))
          .forEach(queue::offer);
    }

    return Flux.fromIterable(results);
  }

  private Stream<IndividualSchedule> addToSchedule(@Nonnull Subject subject,
      @Nonnull IndividualSchedule originalSchedule) {
    return subject.getParallels()
        .stream()
        .filter(parallel -> parallel.getParallelType() != ParallelType.LECTURE)
        .flatMap(parallel -> parallel.getTimetableSlot()
            .stream()
            .map(slot -> new ScheduleEntry(practicalLecturePriority, (short) slot.getDay(),
                (short) slot.getFirstHour(), (short) slot.getDuration(), slot.getRoom(),
                parallel.getTeacher(), parallel.getParallelType(), subject.getName(),
                subject.getCode()))
            .filter(originalSchedule::isSpaceEmpty)
            .map(entry -> {
              IndividualSchedule newSchedule = originalSchedule.getCopy();
              newSchedule.addClass(entry);
              return newSchedule;
            })
        );
  }

  private double evaluateSchedule(@Nonnull IndividualSchedule schedule) {
    double scheduleRating = schedule.getFreeDaysCount() * freeDaysMultiplier +
        (MAXIMUM_SCHEDULE_HOURS - schedule.countHoursInSchool(earliestOKHour, latestOKHour,
            badHourPenalty)) * hoursMultiplier;

    if (separateTheoryLessons) {
      scheduleRating += schedule.getDaysWithOnlyTheoryLessonsCount() * 0.6 * freeDaysMultiplier;
    }

    return scheduleRating;
  }
}
