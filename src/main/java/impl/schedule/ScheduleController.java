package impl.schedule;

import impl.subject.Subject;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * @author Tomáš Hamsa on 18.11.2017.
 */
@RestController
@RequestMapping("/schedule")
class ScheduleController {

  private static final String DEFAULT_THEORY_LESSON_PRIORITY = "2";
  private static final String DEFAULT_PRACTICAL_LECTURE_PRIORITY = "1";
  private static final String DEFAULT_EARLIEST_OK_HOUR = "3";
  private static final String DEFAULT_LATEST_OK_HOUR = "12";
  private static final String DEFAULT_BAD_HOUR_PENALTY = "1.5";
  private static final String DEFAULT_FREE_DAYS_MULTIPLIER = "2.0";
  private static final String DEFAULT_HOURS_MULTIPLIER = "1.0";

  private final ScheduleService scheduleService;

  @Autowired
  ScheduleController(ScheduleService scheduleService) {
    this.scheduleService = scheduleService;
  }

  @ApiOperation(value = "Get a list with all best schedules", response = ScheduleEntry[].class, responseContainer = "List")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "theoryLessonPriority", value = "Priority of theory lessons while building schedules", defaultValue = DEFAULT_THEORY_LESSON_PRIORITY, dataType = "byte", dataTypeClass = byte.class),
      @ApiImplicitParam(name = "practicalLecturePriority", value = "Priority of practical lectures while building schedules", defaultValue = DEFAULT_PRACTICAL_LECTURE_PRIORITY, dataType = "byte", dataTypeClass = byte.class),
      @ApiImplicitParam(name = "earliestOKHour", value = "The earliest hour that will be counted as normal", defaultValue = DEFAULT_EARLIEST_OK_HOUR, dataType = "byte", dataTypeClass = byte.class),
      @ApiImplicitParam(name = "latestOKHour", value = "The last hour that will be counted as normal", defaultValue = DEFAULT_LATEST_OK_HOUR, dataType = "byte", dataTypeClass = byte.class),
      @ApiImplicitParam(name = "badHourPenalty", value = "Amount of hours each hour outside the OK range will be counted for", defaultValue = DEFAULT_BAD_HOUR_PENALTY, dataType = "double", dataTypeClass = double.class),
      @ApiImplicitParam(name = "freeDaysMultiplier", value = "Importance of free days when evaluating schedules", defaultValue = DEFAULT_FREE_DAYS_MULTIPLIER, dataType = "double", dataTypeClass = double.class),
      @ApiImplicitParam(name = "hoursMultiplier", value = "Importance of total hours when evaluating schedules", defaultValue = DEFAULT_HOURS_MULTIPLIER, dataType = "double", dataTypeClass = double.class),
      @ApiImplicitParam(name = "subjects", value = "List of all subjects that will have their parallels placed into the schedules", dataType = "Subject", dataTypeClass = Subject[].class, allowMultiple = true, required = true)
  })
  @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved list of all best schedules"))
  @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  Flux<Collection<ScheduleEntry>> createIndividualSchedule(
      @RequestParam(required = false, defaultValue = DEFAULT_THEORY_LESSON_PRIORITY) short theoryLessonPriority,
      @RequestParam(required = false, defaultValue = DEFAULT_PRACTICAL_LECTURE_PRIORITY) short practicalLecturePriority,
      @RequestParam(required = false, defaultValue = DEFAULT_EARLIEST_OK_HOUR) short earliestOKHour,
      @RequestParam(required = false, defaultValue = DEFAULT_LATEST_OK_HOUR) short latestOKHour,
      @RequestParam(required = false, defaultValue = DEFAULT_BAD_HOUR_PENALTY) double badHourPenalty,
      @RequestParam(required = false, defaultValue = DEFAULT_FREE_DAYS_MULTIPLIER) double freeDaysMultiplier,
      @RequestParam(required = false, defaultValue = DEFAULT_HOURS_MULTIPLIER) double hoursMultiplier,
      @RequestBody Collection<? extends Subject> subjects) {

    return scheduleService.buildIndividualSchedules(theoryLessonPriority, practicalLecturePriority,
        earliestOKHour, latestOKHour, badHourPenalty, freeDaysMultiplier, hoursMultiplier,
        false, subjects);
  }

  @ApiOperation(value = "Get a list with all best schedules that have separated theory lessons", response = ScheduleEntry[].class, responseContainer = "List", notes = "Separated means that if possible there will be days with only theory lessons and no practical lectures")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "theoryLessonPriority", value = "Priority of theory lessons while building schedules", defaultValue = DEFAULT_THEORY_LESSON_PRIORITY, dataType = "byte", dataTypeClass = byte.class),
      @ApiImplicitParam(name = "practicalLecturePriority", value = "Priority of practical lectures while building schedules", defaultValue = DEFAULT_PRACTICAL_LECTURE_PRIORITY, dataType = "byte", dataTypeClass = byte.class),
      @ApiImplicitParam(name = "earliestOKHour", value = "The earliest hour that will be counted as normal", defaultValue = DEFAULT_EARLIEST_OK_HOUR, dataType = "byte", dataTypeClass = byte.class),
      @ApiImplicitParam(name = "latestOKHour", value = "The last hour that will be counted as normal", defaultValue = DEFAULT_LATEST_OK_HOUR, dataType = "byte", dataTypeClass = byte.class),
      @ApiImplicitParam(name = "badHourPenalty", value = "Amount of hours each hour outside the OK range will be counted for", defaultValue = DEFAULT_BAD_HOUR_PENALTY, dataType = "double", dataTypeClass = double.class),
      @ApiImplicitParam(name = "freeDaysMultiplier", value = "Importance of free days when evaluating schedules", defaultValue = DEFAULT_FREE_DAYS_MULTIPLIER, dataType = "double", dataTypeClass = double.class),
      @ApiImplicitParam(name = "hoursMultiplier", value = "Importance of total hours when evaluating schedules", defaultValue = DEFAULT_HOURS_MULTIPLIER, dataType = "double", dataTypeClass = double.class),
      @ApiImplicitParam(name = "subjects", value = "List of all subjects that will have their parallels placed into the schedules", dataType = "Subject", dataTypeClass = Subject[].class, allowMultiple = true, required = true)
  })
  @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved list of all best schedules that have separated theory lessons"))
  @PutMapping(value = "/separate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  Flux<Collection<ScheduleEntry>> createIndividualScheduleSeparated(
      @RequestParam(required = false, defaultValue = DEFAULT_THEORY_LESSON_PRIORITY) short theoryLessonPriority,
      @RequestParam(required = false, defaultValue = DEFAULT_PRACTICAL_LECTURE_PRIORITY) short practicalLecturePriority,
      @RequestParam(required = false, defaultValue = DEFAULT_EARLIEST_OK_HOUR) short earliestOKHour,
      @RequestParam(required = false, defaultValue = DEFAULT_LATEST_OK_HOUR) short latestOKHour,
      @RequestParam(required = false, defaultValue = DEFAULT_BAD_HOUR_PENALTY) double badHourPenalty,
      @RequestParam(required = false, defaultValue = DEFAULT_FREE_DAYS_MULTIPLIER) double freeDaysMultiplier,
      @RequestParam(required = false, defaultValue = DEFAULT_HOURS_MULTIPLIER) double hoursMultiplier,
      @RequestBody Collection<? extends Subject> subjects) {

    return scheduleService.buildIndividualSchedules(theoryLessonPriority, practicalLecturePriority,
        earliestOKHour, latestOKHour, badHourPenalty, freeDaysMultiplier, hoursMultiplier,
        true, subjects);
  }
}