package impl.schedule;

import impl.subject.Subject;
import java.util.Collection;
import javax.annotation.Nonnull;
import reactor.core.publisher.Flux;

/**
 * @author Tomáš Hamsa on 18.11.2017.
 */
@FunctionalInterface
interface ScheduleService {

  Flux<Collection<ScheduleEntry>> buildIndividualSchedules(short theoryLessonPriority,
      short practicalLecturePriority, short earliestOKHour, short latestOKHour,
      double badHourPenalty, double freeDaysMultiplier, double hoursMultiplier,
      boolean separateTheoryLessons, @Nonnull Collection<? extends Subject> subjects);
}
