package impl.schedule;

import com.fasterxml.jackson.annotation.JsonIgnore;
import impl.parallel.ParallelType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Tomáš Hamsa on 25.06.2018.
 */
@ApiModel(description = "One entry in schedule")
class ScheduleEntry implements Serializable {

  private static final long serialVersionUID = -9015154625198415923L;

  @JsonIgnore
  private short priority;
  @ApiModelProperty(value = "Number of day, starting from 1, the entry is taught on", required = true)
  private short day;
  @ApiModelProperty(value = "Number of the first hour of the entry", required = true)
  private short firstHour;
  @ApiModelProperty(value = "Amount of school hour the entry lasts for", required = true)
  private short duration;
  @ApiModelProperty(value = "Code of a room the entry is taught in", required = true)
  private String room;
  @ApiModelProperty("List of all teachers that teach the entry")
  private Collection<String> teacher;
  @ApiModelProperty(value = "Type of the parallel", required = true)
  private ParallelType parallelType;
  @ApiModelProperty(value = "Name of the subject", required = true)
  private String name;
  @ApiModelProperty(value = "Code of the subject as in KOS", required = true)
  private String code;

  public ScheduleEntry() {
    // serialization
  }

  ScheduleEntry(short priority, short day, short firstHour, short duration, String room,
      Collection<String> teacher, ParallelType parallelType, String name, String code) {
    this.priority = priority;
    this.day = day;
    this.firstHour = firstHour;
    this.duration = duration;
    this.room = room;
    this.teacher = teacher;
    this.parallelType = parallelType;
    this.name = name;
    this.code = code;
  }

  short getPriority() {
    return priority;
  }

  public short getDay() {
    return day;
  }

  public short getFirstHour() {
    return firstHour;
  }

  public short getDuration() {
    return duration;
  }

  public String getRoom() {
    return room;
  }

  public Collection<String> getTeacher() {
    return teacher;
  }

  public ParallelType getParallelType() {
    return parallelType;
  }

  public String getName() {
    return name;
  }

  public String getCode() {
    return code;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof ScheduleEntry)) {
      return false;
    }
    ScheduleEntry that = (ScheduleEntry) o;
    return priority == that.priority &&
        day == that.day &&
        firstHour == that.firstHour &&
        duration == that.duration &&
        Objects.equals(room, that.room) &&
        Objects.equals(teacher, that.teacher) &&
        parallelType == that.parallelType &&
        Objects.equals(name, that.name) &&
        Objects.equals(code, that.code);
  }

  @Override
  public int hashCode() {
    return Objects.hash(priority, day, firstHour, duration, room, teacher, parallelType,
        name, code);
  }
}