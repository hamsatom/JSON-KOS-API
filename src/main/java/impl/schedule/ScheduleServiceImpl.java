package impl.schedule;

import impl.subject.Subject;
import java.util.Collection;
import javax.annotation.Nonnull;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

/**
 * @author Tomáš Hamsa on 18.11.2017.
 */
@Service("scheduleService")
class ScheduleServiceImpl implements ScheduleService {

  @Override
  public Flux<Collection<ScheduleEntry>> buildIndividualSchedules(short theoryLessonPriority,
      short practicalLecturePriority, short earliestOKHour, short latestOKHour,
      double badHourPenalty, double freeDaysMultiplier, double hoursMultiplier,
      boolean separateTheoryLessons, @Nonnull Collection<? extends Subject> subjects) {

    IndividualScheduler scheduler = new IndividualScheduler(theoryLessonPriority,
        practicalLecturePriority, earliestOKHour, latestOKHour, badHourPenalty, freeDaysMultiplier,
        hoursMultiplier, separateTheoryLessons);

    return scheduler.findBestSchedule(subjects).map(IndividualSchedule::get);
  }
}
