package impl.schedule;

import impl.parallel.ParallelType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

/**
 * @author Tomáš Hamsa on 11.07.2017.
 */
class IndividualSchedule {

  private static final short SCHOOL_DAYS_COUNT = 5;
  private static final short SCHOOL_HOURS_COUNT = 17;

  private short freeDaysCount;
  private short daysWithOnlyTheoryLessonsCount;
  private boolean[] usedDay;
  private boolean[] dayWithLectures;
  private ScheduleEntry[][] scheduleArray;
  private short[] earliestHour;
  private short[] lastHour;


  IndividualSchedule() {
    daysWithOnlyTheoryLessonsCount = freeDaysCount = SCHOOL_DAYS_COUNT;
    earliestHour = new short[SCHOOL_DAYS_COUNT];
    Arrays.fill(earliestHour, Short.MAX_VALUE);
    lastHour = new short[SCHOOL_DAYS_COUNT];
    usedDay = new boolean[SCHOOL_DAYS_COUNT];
    dayWithLectures = new boolean[SCHOOL_DAYS_COUNT];
    scheduleArray = new ScheduleEntry[SCHOOL_DAYS_COUNT][SCHOOL_HOURS_COUNT];
  }

  @Nonnull
  IndividualSchedule getCopy() {
    IndividualSchedule copy = new IndividualSchedule();
    copy.freeDaysCount = freeDaysCount;
    copy.daysWithOnlyTheoryLessonsCount = daysWithOnlyTheoryLessonsCount;
    copy.usedDay = Arrays.copyOf(usedDay, usedDay.length);
    copy.dayWithLectures = Arrays.copyOf(dayWithLectures, dayWithLectures.length);

    copy.scheduleArray = Arrays.stream(scheduleArray)
        .map(day -> Arrays.copyOf(day, day.length))
        .toArray(ScheduleEntry[][]::new);

    copy.earliestHour = Arrays.copyOf(earliestHour, earliestHour.length);
    copy.lastHour = Arrays.copyOf(lastHour, lastHour.length);

    return copy;
  }


  short getFreeDaysCount() {
    return freeDaysCount;
  }

  short getDaysWithOnlyTheoryLessonsCount() {
    return daysWithOnlyTheoryLessonsCount;
  }

  double countHoursInSchool(short firstOKHour, short lastOKHour, double penalty) {
    double result = 0.0;

    for (short day = 0; day < SCHOOL_DAYS_COUNT; day++) {
      if (lastHour[day] == 0) {
        continue;
      }

      short firstHour = earliestHour[day];
      short latestHour = lastHour[day];

      result += latestHour - firstHour + 1;
      if (latestHour - lastOKHour > 0) {
        result += (latestHour - lastOKHour) * penalty;
      }
      if (firstOKHour - firstHour > 0) {
        result += (firstOKHour - firstHour) * penalty;
      }
    }

    return result;
  }

  @Nonnull
  Collection<ScheduleEntry> get() {
    return Arrays.stream(scheduleArray)
        .flatMap(Arrays::stream)
        .filter(Objects::nonNull)
        .distinct()
        .collect(Collectors.toList());
  }

  void addClass(@Nonnull ScheduleEntry entry) {
    if (!isSpaceEmpty(entry)) {
      return;
    }

    int day = entry.getDay() - 1;
    int startHour = entry.getFirstHour();
    int endHour = startHour + entry.getDuration() - 1;

    // if day was empty, decrease number of free days
    if (!usedDay[day]) {
      --freeDaysCount;
      usedDay[day] = true;
    }

    // if class is a theory lesson, update number of days with only theory lessons
    if (!dayWithLectures[day] && entry.getParallelType() != ParallelType.LECTURE) {
      --daysWithOnlyTheoryLessonsCount;
      dayWithLectures[day] = true;
    }

    for (int hour = startHour; hour <= endHour; hour++) {
      scheduleArray[day][hour] = entry;
    }

    earliestHour[day] = (short) Math.min(earliestHour[day], startHour);
    lastHour[day] = (short) Math.max(lastHour[day], endHour);
  }

  boolean isSpaceEmpty(@Nonnull ScheduleEntry entry) {
    ScheduleEntry[] day = scheduleArray[entry.getDay() - 1];
    short firstHour = entry.getFirstHour();
    return Arrays.stream(day, firstHour, firstHour + entry.getDuration())
        .allMatch(hour -> hour == null || hour.getPriority() < entry.getPriority());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof IndividualSchedule)) {
      return false;
    }
    IndividualSchedule that = (IndividualSchedule) o;
    return freeDaysCount == that.freeDaysCount &&
        daysWithOnlyTheoryLessonsCount == that.daysWithOnlyTheoryLessonsCount &&
        Arrays.equals(usedDay, that.usedDay) &&
        Arrays.equals(dayWithLectures, that.dayWithLectures) &&
        Arrays.deepEquals(scheduleArray, that.scheduleArray) &&
        Arrays.equals(earliestHour, that.earliestHour) &&
        Arrays.equals(lastHour, that.lastHour);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(freeDaysCount, daysWithOnlyTheoryLessonsCount);
    result = 31 * result + Arrays.hashCode(usedDay);
    result = 31 * result + Arrays.hashCode(dayWithLectures);
    result = 31 * result + Arrays.deepHashCode(scheduleArray);
    result = 31 * result + Arrays.hashCode(earliestHour);
    result = 31 * result + Arrays.hashCode(lastHour);
    return result;
  }
}
