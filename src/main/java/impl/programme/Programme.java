package impl.programme;


import impl.shared.ClassesLang;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
@ApiModel(description = "Study programme")
public class Programme implements Serializable {

  private static final long serialVersionUID = -4983042224764632770L;

  @ApiModelProperty("Language the programme is taught at")
  private ClassesLang classesLang;
  @ApiModelProperty(value = "Not unique code of the programme", required = true)
  private String code;
  @ApiModelProperty(value = "Name of the programme", allowEmptyValue = true)
  private String diplomaName;
  @ApiModelProperty(value = "Faculty that teaches the programme", required = true)
  private String faculty;
  @ApiModelProperty(value = "Whenever is the programme open for admission", required = true)
  private boolean openForAdmission;
  @ApiModelProperty(value = "Amount of years needed to complete the programme", required = true)
  private float studyDuration;
  @ApiModelProperty(value = "Type of programme the programme is focused for", required = true)
  private ProgrammeType type;

  public ClassesLang getClassesLang() {
    return classesLang;
  }

  public String getCode() {
    return code;
  }

  public String getDiplomaName() {
    return diplomaName;
  }

  public String getFaculty() {
    return faculty;
  }

  public boolean isOpenForAdmission() {
    return openForAdmission;
  }

  public float getStudyDuration() {
    return studyDuration;
  }

  public ProgrammeType getType() {
    return type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Programme)) {
      return false;
    }
    Programme programme = (Programme) o;
    return openForAdmission == programme.openForAdmission &&
        Float.compare(programme.studyDuration, studyDuration) == 0 &&
        classesLang == programme.classesLang &&
        Objects.equals(code, programme.code) &&
        Objects.equals(diplomaName, programme.diplomaName) &&
        Objects.equals(faculty, programme.faculty) &&
        type == programme.type;
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(classesLang, code, diplomaName, faculty, openForAdmission, studyDuration, type);
  }
}