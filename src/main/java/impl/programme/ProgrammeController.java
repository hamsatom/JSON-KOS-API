package impl.programme;

import impl.kos.ApiBase;
import impl.shared.ClassesLang;
import impl.shared.Constants;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * @author Tomáš Hamsa on 18.11.2017.
 */
@RestController
@RequestMapping("/programme")
class ProgrammeController {

  private final ProgrammeService programmeService;

  @Autowired
  ProgrammeController(ProgrammeService programmeService) {
    this.programmeService = programmeService;
  }

  @ApiOperation(value = "Get a list with all programmes", response = Programme.class, responseContainer = "List")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "semester", value = "Semester in which the programmes are taught", required = true),
      @ApiImplicitParam(name = "language", value = "Language of the programmes", allowEmptyValue = true),
      @ApiImplicitParam(name = "code", value = "Code of the programmes", allowEmptyValue = true),
      @ApiImplicitParam(name = "duration", value = "Amount of years it takes to complete the programmes", dataType = "Float", dataTypeClass = Float.class, allowEmptyValue = true),
      @ApiImplicitParam(name = "faculty", value = "Faculty that teaches the programmes", allowEmptyValue = true),
      @ApiImplicitParam(name = "name", value = "Name of the programmes", allowEmptyValue = true),
      @ApiImplicitParam(name = "type", value = "Programme type the programmes are focused for", allowEmptyValue = true),
      @ApiImplicitParam(name = "open", value = "Programmes are open for admission", dataType = "Boolean", dataTypeClass = Boolean.class, allowEmptyValue = true),
      @ApiImplicitParam(name = "apiBase", value = "KOS API location", defaultValue = Constants.DEFAULT_API)
  })
  @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved list of all programmes"))
  @GetMapping(value = "/{semester}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  Flux<Programme> obtainProgrammes(@PathVariable String semester,
      @RequestParam(required = false) ClassesLang language,
      @RequestParam(required = false) String code, @RequestParam(required = false) Float duration,
      @RequestParam(required = false) String faculty, @RequestParam(required = false) String name,
      @RequestParam(required = false) ProgrammeType type,
      @RequestParam(required = false) Boolean open,
      @RequestParam(required = false, defaultValue = Constants.DEFAULT_API) ApiBase apiBase) {

    return programmeService.downloadProgrammes(semester, language, code, faculty, name, type, open,
        duration, apiBase);
  }
}
