package impl.programme;

import impl.kos.ApiBase;
import impl.shared.ClassesLang;
import javax.annotation.Nonnull;
import reactor.core.publisher.Flux;

/**
 * @author Tomáš Hamsa on 18.11.2017.
 */
@FunctionalInterface
interface ProgrammeService {

  Flux<Programme> downloadProgrammes(@Nonnull String semester, ClassesLang language, String code,
      String faculty, String name, ProgrammeType type, Boolean open, Float duration,
      @Nonnull ApiBase apiBase);
}
