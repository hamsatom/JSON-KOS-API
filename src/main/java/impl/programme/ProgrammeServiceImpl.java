package impl.programme;

import impl.kos.ApiBase;
import impl.kos.Kos;
import impl.shared.ClassesLang;
import impl.shared.QueryBuilder;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

/**
 * @author Tomáš Hamsa on 18.11.2017.
 */
@Service("programmeService")
class ProgrammeServiceImpl implements ProgrammeService {

  private static final Logger LOG = Logger.getLogger(ProgrammeServiceImpl.class.getName());

  @Override
  public Flux<Programme> downloadProgrammes(@Nonnull String semester, ClassesLang language,
      String code, String faculty, String name, ProgrammeType type, Boolean open, Float duration,
      @Nonnull ApiBase apiBase) {

    String query = QueryBuilder.buildQuery(
        "classesLang", language,
        "code", code,
        "diplomaName", name,
        "faculty.name", faculty,
        "openForAdmission", open,
        "studyDuration", duration,
        "type", type);

    try {
      return Kos.getAllProgrammes(apiBase, semester, query);
    } catch (IOException e) {
      LOG.log(Level.SEVERE, "programmes: " + query + semester + apiBase, e);
      throw new IllegalStateException("Exception while getting programmes from KOS", e);
    }
  }
}
