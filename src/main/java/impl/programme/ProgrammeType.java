package impl.programme;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
public enum ProgrammeType {
  BACHELOR, DOCTORAL, INTERNSHIP, LIFELONG, MASTER, MASTER_LEGACY, UNDEFINED
}
