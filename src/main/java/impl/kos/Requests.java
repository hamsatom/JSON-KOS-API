package impl.kos;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;


final class Requests {

  private static final String PARAMETERS = "?detail=0&lang=cs&limit=262&locEnums=false&multilang=false&sem=";
  private static final Pattern PATTERN_PLUS = Pattern.compile("\\+");
  private static final Pattern PATTERN_HASH = Pattern.compile("#");

  private Requests() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  private static String createQuery(@Nullable String query) throws URIException {
    if (StringUtils.isBlank(query)) {
      return "";
    }

    String rawQuery = "&query=" + query;
    return URIUtil.encodeQuery(rawQuery, Constants.CHARSET.name());
  }

  static String getStudentCourses(@Nonnull ApiBase location, @Nonnull String studentUsernameOrId,
      String semesterCode) throws IOException {

    return doGetRequest(
        location.getApiURL() + "students/" + studentUsernameOrId.toLowerCase() + "/enrolledCourses"
            + PARAMETERS + semesterCode + createQuery("completed==false")
            + "&fields=entry/content/course"
    );
  }

  static String getCourseParallels(@Nonnull ApiBase location, String courseCode,
      String semesterCode, @Nullable String query) throws IOException {

    return doGetRequest(
        location.getApiURL() + "courses/" + courseCode + "/parallels" + PARAMETERS + semesterCode
            + createQuery(query)
            + "&fields=entry/content(capacity,capacityOverfill,occupied,parallelType,teacher,teachers,timetableSlot(day,duration,firstHour,room))"
    );
  }

  static String getCourseName(@Nonnull ApiBase location, String courseCode, String semesterCode)
      throws IOException {

    return doGetRequest(location.getApiURL() + "courses/" + courseCode + PARAMETERS + semesterCode
        + "&fields=content/name");
  }

  static String getProgrammeCourses(@Nonnull ApiBase location, String query, String programmeCode,
      String semesterCode) throws IOException {

    String convertedQuery = createQuery(query);
    convertedQuery =
        convertedQuery.isEmpty() ? "&query=state!=CLOSED" : convertedQuery + ";state!=CLOSED";

    return doGetRequest(
        location.getApiURL() + "programmes/" + programmeCode + "/courses" + PARAMETERS
            + semesterCode + convertedQuery
            + "&fields=entry/content(classesLang,code,completion,credits,department,name,programmeType,season,studyForm)");
  }

  static String getAllProgrammes(@Nonnull ApiBase location, @Nullable String query,
      String semesterCode) throws IOException {

    return doGetRequest(
        location.getApiURL() + "programmes" + PARAMETERS + semesterCode + createQuery(query)
            + "&fields=entry/content(classesLang,code,diplomaName,faculty,openForAdmission,studyDuration,type)");
  }

  static String getAllCourses(@Nonnull ApiBase location, @Nullable String query,
      @Nonnull String semesterCode) throws IOException {

    String convertedQuery = createQuery(query);
    convertedQuery =
        convertedQuery.isEmpty() ? "&query=state!=CLOSED" : convertedQuery + ";state!=CLOSED";

    return doGetRequest(
        location.getApiURL() + "courses" + PARAMETERS + semesterCode + convertedQuery
            + "&fields=entry/content(classesLang,code,completion,credits,department,name,programmeType,season,studyForm,capacity)");
  }

  private static String doGetRequest(@Nonnull String url) throws IOException {
    String token = Authentication.getToken();

    url = PATTERN_PLUS.matcher(url).replaceAll("%2B");
    url = PATTERN_HASH.matcher(url).replaceAll("%23");
    HttpUriRequest httpGet = new HttpGet(url);
    httpGet.addHeader("Authorization", " Bearer " + token);
    httpGet.addHeader("Accept",
        "application/atom+xml, application/atom+xml;type=entry, application/atom+xml;type=feed");

    try (CloseableHttpClient client = HttpClientBuilder.create()
        .setConnectionTimeToLive(Constants.TIMEOUT, TimeUnit.MILLISECONDS).build()) {

      HttpResponse response = client.execute(httpGet);

      try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
        byte[] buffer = new byte[4096];
        int length;
        try (InputStream inStream = response.getEntity().getContent()) {
          while ((length = inStream.read(buffer)) != -1) {
            byteArrayOutputStream.write(buffer, 0, length);
          }
        }

        return byteArrayOutputStream.toString(Constants.CHARSET.name());
      }
    }

  }
}

