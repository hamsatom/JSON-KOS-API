package impl.kos;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @author Tomáš Hamsa on 13.09.2017.
 */
final class Constants {

  static final int TIMEOUT = 1000;
  static final Charset CHARSET = StandardCharsets.UTF_8;

  private Constants() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing constants holder");
  }
}