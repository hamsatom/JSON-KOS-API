package impl.subject;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
public class InternalCourseEnrollment implements Serializable {

  private static final long serialVersionUID = 827016906268843329L;

  private InternalCourse course;

  String getCourseCode() {
    String code = course.courseCode;
    if (code.startsWith("courses/") && code.endsWith("/")) {
      code = code.substring(8, code.length() - 1);
      course.courseCode = code;
    }
    return code;
  }

  String getName() {
    return course.name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof InternalCourseEnrollment)) {
      return false;
    }
    InternalCourseEnrollment that = (InternalCourseEnrollment) o;
    return Objects.equals(course, that.course);
  }

  @Override
  public int hashCode() {
    return Objects.hash(course);
  }

  public static final class InternalCourse implements Serializable {

    private static final long serialVersionUID = 89406518843329L;

    private String courseCode;
    private String name;

    private InternalCourse() throws IllegalAccessException {
      throw new IllegalAccessException();
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof InternalCourse)) {
        return false;
      }
      InternalCourse that = (InternalCourse) o;
      return Objects.equals(courseCode, that.courseCode) &&
          Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
      return Objects.hash(courseCode, name);
    }
  }
}
