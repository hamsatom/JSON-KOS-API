package impl.subject;

import impl.kos.ApiBase;
import impl.shared.Constants;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Tomáš Hamsa on 18.11.2017.
 */
@RestController
@RequestMapping("/subject")
class SubjectController {

  private final SubjectService subjectService;

  @Autowired
  SubjectController(SubjectService subjectService) {
    this.subjectService = subjectService;
  }

  @ApiOperation(value = "Get a list with student's all not completed subjects", response = Subject.class, responseContainer = "List")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "semester", value = "Semester in which the subjects are taught", required = true),
      @ApiImplicitParam(name = "student", value = "Student's code (KOS login) or id", required = true),
      @ApiImplicitParam(name = "apiBase", value = "KOS API location", defaultValue = Constants.DEFAULT_API)
  })
  @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved list of student's all not completed subjects"))
  @GetMapping(value = "/{semester}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public Flux<Subject> studentSubjects(@PathVariable String semester,
      @RequestParam String student,
      @RequestParam(required = false, defaultValue = Constants.DEFAULT_API) ApiBase apiBase) {

    return subjectService.downloadStudentSubjects(semester, student, apiBase);
  }

  @ApiOperation(value = "Get a subject with all parallels", response = Subject.class)
  @ApiImplicitParams({
      @ApiImplicitParam(name = "semester", value = "Semester in which the subjects are taught", required = true),
      @ApiImplicitParam(name = "courseCode", value = "Code of the subject", required = true),
      @ApiImplicitParam(name = "apiBase", value = "KOS API location", defaultValue = Constants.DEFAULT_API)
  })
  @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved subject with all parallels"))
  @GetMapping(value = "/{semester}/{courseCode}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  Mono<Subject> allSubjects(@PathVariable String semester, @PathVariable String courseCode,
      @RequestParam(required = false, defaultValue = Constants.DEFAULT_API) ApiBase apiBase) {

    return subjectService.downloadSubjects(semester, courseCode, apiBase);
  }

  @ApiOperation(value = "Get a subject with all open parallels", response = Subject.class)
  @ApiImplicitParams({
      @ApiImplicitParam(name = "semester", value = "Semester in which the subjects are taught", required = true),
      @ApiImplicitParam(name = "courseCode", value = "Code of the subject", required = true),
      @ApiImplicitParam(name = "apiBase", value = "KOS API location", defaultValue = Constants.DEFAULT_API)
  })
  @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved subject with all open parallels"))
  @GetMapping(value = "/{semester}/{courseCode}/open", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  Mono<Subject> openSubjects(@PathVariable String semester, @PathVariable String courseCode,
      @RequestParam(required = false, defaultValue = Constants.DEFAULT_API) ApiBase apiBase) {

    return subjectService.downloadOpenSubjects(semester, courseCode, apiBase);
  }

  @ApiOperation(value = "Get a subject with all not full parallels", response = Subject.class)
  @ApiImplicitParams({
      @ApiImplicitParam(name = "semester", value = "Semester in which the subjects are taught", required = true),
      @ApiImplicitParam(name = "courseCode", value = "Code of the subject", required = true),
      @ApiImplicitParam(name = "apiBase", value = "KOS API location", defaultValue = Constants.DEFAULT_API)
  })
  @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved subject with all not full parallels"))
  @GetMapping(value = "/{semester}/{courseCode}/notFull", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  Mono<Subject> notFullSubjects(@PathVariable String semester, @PathVariable String courseCode,
      @RequestParam(required = false, defaultValue = Constants.DEFAULT_API) ApiBase apiBase) {

    return subjectService.downloadNotFullSubjects(semester, courseCode, apiBase);
  }

  @ApiOperation(value = "Get a subject with all not full and open parallels", response = Subject.class)
  @ApiImplicitParams({
      @ApiImplicitParam(name = "semester", value = "Semester in which the subjects are taught", required = true),
      @ApiImplicitParam(name = "courseCode", value = "Code of the subject", required = true),
      @ApiImplicitParam(name = "apiBase", value = "KOS API location", defaultValue = Constants.DEFAULT_API)
  })
  @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved subject with all not full and open parallels"))
  @GetMapping(value = "/{semester}/{courseCode}/open/notFull", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  Mono<Subject> openNotFullSubjects(@PathVariable String semester, @PathVariable String courseCode,
      @RequestParam(required = false, defaultValue = Constants.DEFAULT_API) ApiBase apiBase) {

    return subjectService.downloadOpenNotFullSubjects(semester, courseCode, apiBase);
  }

}
