package impl.subject;

import impl.kos.ApiBase;
import javax.annotation.Nonnull;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

interface SubjectService {

  Flux<Subject> downloadStudentSubjects(@Nonnull String semester, @Nonnull String studentCode,
      @Nonnull ApiBase apiBase);

  Mono<Subject> downloadOpenNotFullSubjects(@Nonnull String semester, @Nonnull String courseCode,
      @Nonnull ApiBase apiBase);

  Mono<Subject> downloadOpenSubjects(@Nonnull String semester, @Nonnull String courseCode,
      @Nonnull ApiBase apiBase);

  Mono<Subject> downloadNotFullSubjects(@Nonnull String semester, @Nonnull String courseCode,
      @Nonnull ApiBase apiBase);

  Mono<Subject> downloadSubjects(@Nonnull String semester, @Nonnull String courseCode,
      @Nonnull ApiBase apiBase);
}
