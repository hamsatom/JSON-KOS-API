package impl.subject;

import impl.parallel.Parallel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Tomáš Hamsa on 17.06.2018.
 */
@SuppressWarnings("unused")
@ApiModel(description = "Course with parallels")
public class Subject implements Serializable {

  private static final long serialVersionUID = 2570679914994869596L;

  @ApiModelProperty(value = "Name of the subject", required = true)
  private String name;
  @ApiModelProperty(value = "Code of the subject as in KOS", required = true)
  private String code;
  @ApiModelProperty(value = "List with parallels of the subject", required = true)
  private Collection<Parallel> parallels;

  public Subject() {
    // serialization
  }

  public Subject(String name, String code, Collection<Parallel> parallels) {
    this.name = name;
    this.code = code;
    this.parallels = parallels;
  }

  public String getName() {
    return name;
  }

  public Subject setName(String name) {
    this.name = name;
    return this;
  }

  public String getCode() {
    return code;
  }

  public Subject setCode(String code) {
    this.code = code;
    return this;
  }

  public Collection<Parallel> getParallels() {
    return parallels;
  }

  public Subject setParallels(Collection<Parallel> parallels) {
    this.parallels = parallels;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Subject)) {
      return false;
    }
    Subject subject = (Subject) o;
    return Objects.equals(name, subject.name) &&
        Objects.equals(code, subject.code) &&
        Objects.equals(parallels, subject.parallels);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, code, parallels);
  }
}
