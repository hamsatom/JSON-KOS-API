package impl.subject;

import impl.kos.ApiBase;
import impl.kos.Kos;
import impl.parallel.Parallel;
import impl.shared.Permission;
import java.io.IOException;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

/**
 * @author Tomáš Hamsa on 17.06.2018.
 */
@Service("subjectService")
class SubjectServiceImpl implements SubjectService {

  private static final Logger LOG = Logger.getLogger(SubjectServiceImpl.class.getName());
  private static final String QUERY_NOT_CLOSED = "(enrollment==ALLOWED,parallelType==LECTURE)";
  private static final Predicate<Parallel> FILTER_FULL = parallel ->
      parallel.getOccupied() < parallel.getCapacity()
          || parallel.getCapacityOverfill() == Permission.ALLOWED;

  private static Mono<Subject> downloadSubjects(@Nonnull String semester,
      @Nonnull String courseCode, @Nonnull ApiBase apiBase, String query,
      @Nullable Predicate<Parallel> filter) {
    try {
      String courseName = Kos.getCourseName(apiBase, semester, courseCode);
      Flux<Parallel> pars = Kos.getCourseParallels(apiBase, semester, courseCode, query);
      if (filter != null) {
        pars = pars.filter(filter);
      }
      return pars.collectList()
          .map(parallels -> new Subject(courseName, courseCode, parallels));
    } catch (IOException e) {
      LOG.log(Level.SEVERE, "subject:" + semester + courseCode + query + apiBase, e);
      throw new IllegalStateException("Exception while getting subjects from KOS", e);
    }
  }

  @Override
  public Flux<Subject> downloadStudentSubjects(@Nonnull String semester,
      @Nonnull String studentCode, @Nonnull ApiBase apiBase) {
    try {
      return Kos.getStudentCourses(apiBase, semester, studentCode).parallel()
          .runOn(Schedulers.elastic())
          .map(enrollment -> {
                try {
                  return Kos.getCourseParallels(apiBase, semester, enrollment.getCourseCode(), null)
                      .collectList()
                      .map(parallels -> new Subject(enrollment.getName(), enrollment.getCourseCode(),
                          parallels))
                      .block();
                } catch (IOException e) {
                  LOG.log(Level.SEVERE,
                      "student's subject:" + studentCode + semester + enrollment.getCourseCode()
                          + enrollment.getName() + apiBase, e);
                  throw new IllegalStateException("Exception while getting student's subjects from KOS",
                      e);
                }
              }
          ).sequential();
    } catch (IOException e) {
      LOG.log(Level.SEVERE, "student's subject:" + studentCode + semester + apiBase, e);
      throw new IllegalStateException("Exception while getting student's subjects from KOS", e);
    }
  }

  @Override
  public Mono<Subject> downloadOpenNotFullSubjects(@Nonnull String semester,
      @Nonnull String courseCode, @Nonnull ApiBase apiBase) {
    return downloadSubjects(semester, courseCode, apiBase, QUERY_NOT_CLOSED, FILTER_FULL);
  }

  @Override
  public Mono<Subject> downloadOpenSubjects(@Nonnull String semester, @Nonnull String courseCode,
      @Nonnull ApiBase apiBase) {
    return downloadSubjects(semester, courseCode, apiBase, QUERY_NOT_CLOSED, null);
  }

  @Override
  public Mono<Subject> downloadNotFullSubjects(@Nonnull String semester, @Nonnull String courseCode,
      @Nonnull ApiBase apiBase) {
    return downloadSubjects(semester, courseCode, apiBase, null, FILTER_FULL);
  }

  @Override
  public Mono<Subject> downloadSubjects(@Nonnull String semester, @Nonnull String courseCode,
      @Nonnull ApiBase apiBase) {
    return downloadSubjects(semester, courseCode, apiBase, null, null);
  }
}
