package impl.notification;

import impl.kos.ApiBase;
import impl.parallel.ParallelType;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.annotation.Nonnull;

/**
 * @author Tomáš Hamsa on 14.07.2018.
 */
class Recipient {

  private final String email;
  private final String courseCode;
  private final ParallelType parallelType;
  private final byte day;
  private final byte firstHour;
  private final byte lastHour;
  private final String semester;
  private final ApiBase apiBase;
  private final LocalDateTime dateCreated;

  Recipient(@Nonnull String email, @Nonnull String courseCode, @Nonnull ParallelType parallelType,
      byte day, byte firstHour, byte lastHour, @Nonnull String semester, @Nonnull ApiBase apiBase) {
    dateCreated = LocalDateTime.now();
    this.email = email;
    this.courseCode = courseCode;
    this.parallelType = parallelType;
    this.day = day;
    this.firstHour = firstHour;
    this.lastHour = lastHour;
    this.semester = semester;
    this.apiBase = apiBase;
  }

  String getEmail() {
    return email;
  }

  String getCourseCode() {
    return courseCode;
  }

  ParallelType getParallelType() {
    return parallelType;
  }

  byte getDay() {
    return day;
  }

  byte getFirstHour() {
    return firstHour;
  }

  byte getLastHour() {
    return lastHour;
  }

  String getSemester() {
    return semester;
  }

  ApiBase getApiBase() {
    return apiBase;
  }

  LocalDateTime getDateCreated() {
    return dateCreated;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Recipient)) {
      return false;
    }
    Recipient recipient = (Recipient) o;
    return day == recipient.day &&
        firstHour == recipient.firstHour &&
        lastHour == recipient.lastHour &&
        Objects.equals(email, recipient.email) &&
        Objects.equals(courseCode, recipient.courseCode) &&
        parallelType == recipient.parallelType &&
        Objects.equals(semester, recipient.semester) &&
        apiBase == recipient.apiBase;
  }

  @Override
  public int hashCode() {
    return Objects.hash(email, courseCode, parallelType, day, firstHour, lastHour, semester,
        apiBase);
  }

  @Override
  public String toString() {
    return "Recipient{" +
        "email='" + email + '\'' +
        ", courseCode='" + courseCode + '\'' +
        ", parallelType=" + parallelType +
        ", day=" + day +
        ", firstHour=" + firstHour +
        ", lastHour=" + lastHour +
        ", semester='" + semester + '\'' +
        ", apiBase=" + apiBase +
        ", dateCreated=" + dateCreated +
        '}';
  }
}
