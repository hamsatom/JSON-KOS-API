package impl.notification;

import impl.kos.ApiBase;
import impl.parallel.ParallelType;
import javax.annotation.Nonnull;

@FunctionalInterface
interface NotificationService {

  void submitReceiver(@Nonnull String email, @Nonnull String courseCode,
      @Nonnull ParallelType parallelType, byte day, byte firstHour, byte lastHour,
      @Nonnull String semester, @Nonnull ApiBase apiBase);
}
