package impl.notification;

import impl.kos.ApiBase;
import impl.parallel.ParallelType;
import impl.shared.Constants;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Tomáš Hamsa on 14.07.2018.
 */
@RestController
@RequestMapping("/notification")
class NotificationController {

  private static final String DEFAULT_PARALLEL_TYPE = "TUTORIAL";

  private final NotificationService notificationService;

  @Autowired
  NotificationController(NotificationService notificationService) {
    this.notificationService = notificationService;
  }

  @ApiOperation("Subscribe for email notification when it's possible to enroll in a parallel")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "semester", value = "Semester in which the parallel is taught", required = true),
      @ApiImplicitParam(name = "courseCode", value = "Code of the course in which parallel is taught", required = true),
      @ApiImplicitParam(name = "day", value = "Number of the day, starting from 1, in which parallel is taught", dataType = "byte", dataTypeClass = byte.class, required = true),
      @ApiImplicitParam(name = "firstHour", value = "The first hour of parallel", dataType = "byte", dataTypeClass = byte.class, required = true),
      @ApiImplicitParam(name = "lastHour", value = "The last hour of parallel", dataType = "byte", dataTypeClass = byte.class, required = true),
      @ApiImplicitParam(name = "email", value = "Email on which the notification will be sent", required = true),
      @ApiImplicitParam(name = "parallelType", value = "Type of parallel ", defaultValue = DEFAULT_PARALLEL_TYPE),
      @ApiImplicitParam(name = "apiBase", value = "KOS API location", defaultValue = Constants.DEFAULT_API)
  })
  @ApiResponses({
      @ApiResponse(code = 204, message = "Successfully subscribed for email notification"),
      @ApiResponse(code = 400, message = "Request parameters aren't valid")
  })
  @PostMapping("/{semester}/{courseCode}")
  ResponseEntity<Object> signForNotification(@PathVariable String semester,
      @PathVariable String courseCode, @RequestParam byte day, @RequestParam byte firstHour,
      @RequestParam byte lastHour, @RequestParam String email,
      @RequestParam(required = false, defaultValue = DEFAULT_PARALLEL_TYPE) ParallelType parallelType,
      @RequestParam(required = false, defaultValue = Constants.DEFAULT_API) ApiBase apiBase) {

    if (day > 7 || day < 1 || firstHour < 0 || firstHour > lastHour || StringUtils.isAnyBlank(email,
        courseCode, courseCode)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    try {
      new InternetAddress(email).validate();
    } catch (AddressException e) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    notificationService.submitReceiver(email, courseCode, parallelType, day, firstHour, lastHour,
        semester, apiBase);

    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
