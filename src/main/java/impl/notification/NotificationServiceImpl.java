package impl.notification;

import impl.kos.ApiBase;
import impl.kos.Kos;
import impl.parallel.ParallelType;
import impl.shared.Permission;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @author Tomáš Hamsa on 14.07.2018.
 */
@Service("notificationService")
class NotificationServiceImpl implements NotificationService {

  private static final Logger LOG = Logger.getLogger(NotificationServiceImpl.class.getName());
  private static final byte MONTHS_LIMIT = 5;
  private static final String ALLOWED_ENROLLMENT = "enrollment==ALLOWED";

  private final MailSender mailSender;
  private final Set<Recipient> recipients;

  @Autowired
  NotificationServiceImpl(MailSender mailSender) {
    this.mailSender = mailSender;
    recipients = new HashSet<>();
  }

  private static boolean canEnroll(@Nonnull Recipient recipient) throws IOException {
    return Boolean.TRUE == Kos.getCourseParallels(recipient.getApiBase(), recipient.getSemester(),
        recipient.getCourseCode(), ALLOWED_ENROLLMENT)
        .any(parallel -> (parallel.getCapacity() > parallel.getOccupied()
            || parallel.getCapacityOverfill() == Permission.ALLOWED)
            && parallel.getTimetableSlot()
            .stream()
            .anyMatch(slot -> recipient.getDay() == slot.getDay()
                && recipient.getFirstHour() == slot.getFirstHour()
                && recipient.getLastHour() == slot.getFirstHour() + slot.getDuration() - 1))
        .block();
  }

  @Override
  public void submitReceiver(@Nonnull String email, @Nonnull String courseCode,
      @Nonnull ParallelType parallelType, byte day, byte firstHour, byte lastHour,
      @Nonnull String semester, @Nonnull ApiBase apiBase) {

    Recipient recipient = new Recipient(email, courseCode, parallelType, day, firstHour, lastHour,
        semester, apiBase);
    recipients.add(recipient);
  }

  @Scheduled(fixedDelay = 1000)
  private void notifyWhenEnrollmentPossible() {
    Collection<Recipient> notNotifiedRecipients = recipients.parallelStream()
        .filter(recipient -> recipient.getDateCreated()
            .isAfter(LocalDateTime.now().minusMonths(MONTHS_LIMIT)))
        .filter(recipient -> {
          try {
            if (canEnroll(recipient)) {
              sendEmail(recipient);
              return false;
            }
          } catch (Exception e) {
            LOG.log(Level.SEVERE, "notification: " + recipient, e);
          }
          return true;
        })
        .collect(Collectors.toSet());

    recipients.clear();
    recipients.addAll(notNotifiedRecipients);
  }

  private void sendEmail(@Nonnull Recipient recipient) {
    SimpleMailMessage emailMessage = new SimpleMailMessage();
    emailMessage.setTo(recipient.getEmail());
    emailMessage.setSubject("Uvolněn zápis do " + recipient.getCourseCode());
    emailMessage.setText(
        "Lze se zapsat na " + recipient.getParallelType().getValue() + " předmětu " + recipient
            .getCourseCode() + " vyučované " + recipient.getDay() + ". den v týdnu od " + recipient
            .getFirstHour() + ". do " + recipient.getLastHour() + ". vyučovací hodiny v semestru "
            + recipient.getSemester()
            + System.lineSeparator() + System.lineSeparator() +
            "Tento email byl automaticky poslán z JSON KOS API po přihlášení k notifikaci "
            + recipient.getDateCreated() + " s adresou " + recipient.getEmail()
            + System.lineSeparator()
            + "Projekt: https://gitlab.fel.cvut.cz/hamsatom/JSON-KOS-API"
            + System.lineSeparator()
            + "Dokumentace API: https://json-kos-api.193b.starter-ca-central-1.openshiftapps.com/swagger-ui.html"
            + System.lineSeparator()
            + "Adresa API: https://json-kos-api.193b.starter-ca-central-1.openshiftapps.com"
    );

    mailSender.send(emailMessage);
  }
}
