package impl.shared;

import java.util.StringJoiner;
import javax.annotation.Nonnull;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Tomáš Hamsa on 19.06.2018.
 */
public final class QueryBuilder {

  private QueryBuilder() throws IllegalAccessException {
    throw new IllegalAccessException("Single instance protection");
  }

  public static String buildQuery(@Nonnull Object... queryPairs) {
    if (queryPairs.length % 2 != 0) {
      throw new IllegalArgumentException("Not all pairs have key and value");
    }

    StringJoiner query = new StringJoiner(";");

    for (int i = 0; i < queryPairs.length - 1; i += 2) {

      Object key = queryPairs[i];
      Object value = queryPairs[i + 1];

      String valueString;
      if (value == null) {
        valueString = null;
      } else if (value instanceof CharSequence && StringUtils.isNotBlank((CharSequence) value)) {
        valueString = "'*" + value + "*'";
      } else if (value instanceof Enum<?>) {
        valueString = ((Enum) value).name();
      } else {
        valueString = value.toString();
      }

      if (StringUtils.isNotBlank(valueString)) {
        query.add(String.format("%s==%s", key, valueString));
      }
    }

    return query.toString();
  }

}
