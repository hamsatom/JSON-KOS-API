package impl.shared;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
public enum Permission {
  ALLOWED, DENIED, UNDEFINED
}
