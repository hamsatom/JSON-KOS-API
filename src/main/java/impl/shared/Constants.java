package impl.shared;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
public final class Constants {

  public static final String DEFAULT_API = "FEL";

  private Constants() throws IllegalAccessException {
    throw new IllegalAccessException("Single instance protection");
  }
}
