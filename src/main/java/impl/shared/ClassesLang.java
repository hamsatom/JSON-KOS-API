package impl.shared;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
public enum ClassesLang {
  CS, DE, EN, ES, FR, PL, RU, SK, UNDEFINED
}
