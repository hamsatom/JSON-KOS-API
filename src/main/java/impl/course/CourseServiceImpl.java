package impl.course;

import impl.kos.ApiBase;
import impl.kos.Kos;
import impl.programme.ProgrammeType;
import impl.shared.ClassesLang;
import impl.shared.QueryBuilder;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

/**
 * @author Tomáš Hamsa on 18.11.2017.
 */
@Service("courseService")
class CourseServiceImpl implements CourseService {

  private static final Logger LOG = Logger.getLogger(CourseServiceImpl.class.getName());

  @Override
  public Flux<Course> downloadProgrammeCourses(@Nonnull String semester,
      @Nonnull String programmeCode, ClassesLang language, String code, Completion completion,
      Byte credits, String department, String name, ProgrammeType type, Season season,
      StudyForm form, @Nonnull ApiBase apiBase) {

    String query = QueryBuilder.buildQuery(
        "classesLang", language,
        "code", code,
        "completion", completion,
        "credits", credits,
        "department.name", department,
        "name", name,
        "programmeType", type,
        "season", season,
        "studyForm", form);

    try {
      return Kos.getProgrammeCourses(apiBase, semester, query, programmeCode);
    } catch (IOException e) {
      LOG.log(Level.SEVERE, "programme course:" + programmeCode + query + semester + apiBase, e);
      throw new IllegalStateException("Exception while getting programme's courses from KOS", e);
    }
  }

  @Override
  public Flux<Course> downloadCourses(@Nonnull String semester, ClassesLang language, String code,
      Completion completion, Byte credits, String department, String name, ProgrammeType type,
      Season season, StudyForm form, @Nonnull ApiBase apiBase) {

    String query = QueryBuilder.buildQuery(
        "classesLang", language,
        "code", code,
        "completion", completion,
        "credits", credits,
        "department.name", department,
        "name", name,
        "programmeType", type,
        "season", season,
        "studyForm", form);

    try {
      return Kos.getAllCourses(apiBase, semester, query);
    } catch (IOException e) {
      LOG.log(Level.SEVERE, "course:" + query + semester + apiBase, e);
      throw new IllegalStateException("Exception while getting courses from KOS", e);
    }
  }
}