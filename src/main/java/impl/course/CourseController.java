package impl.course;

import impl.kos.ApiBase;
import impl.programme.ProgrammeType;
import impl.shared.ClassesLang;
import impl.shared.Constants;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * @author Tomáš Hamsa on 18.11.2017.
 */
@RestController
@RequestMapping("/course")
class CourseController {

  private final CourseService courseService;

  @Autowired
  CourseController(CourseService courseService) {
    this.courseService = courseService;
  }

  @ApiOperation(value = "Get a list with all programme's courses", response = Course.class, responseContainer = "List")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "semester", value = "Semester in which the courses are taught", required = true),
      @ApiImplicitParam(name = "programme", value = "Code of the programme that has the courses", required = true),
      @ApiImplicitParam(name = "language", value = "Language of the courses", allowEmptyValue = true),
      @ApiImplicitParam(name = "code", value = "Code of the courses as in KOS", allowEmptyValue = true),
      @ApiImplicitParam(name = "completion", value = "Type of completion", allowEmptyValue = true),
      @ApiImplicitParam(name = "credits", value = "Amount of credits", dataType = "Byte", dataTypeClass = Byte.class, allowEmptyValue = true),
      @ApiImplicitParam(name = "department", value = "Department that teaches the courses", allowEmptyValue = true),
      @ApiImplicitParam(name = "name", value = "Name of the courses", allowEmptyValue = true),
      @ApiImplicitParam(name = "type", value = "Programme type the courses are focused for", allowEmptyValue = true),
      @ApiImplicitParam(name = "season", value = "Type  of semester", allowEmptyValue = true),
      @ApiImplicitParam(name = "form", value = "Study form  the courses are focused for", allowEmptyValue = true),
      @ApiImplicitParam(name = "apiBase", value = "KOS API location", defaultValue = Constants.DEFAULT_API)
  })
  @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved list of all programme's courses"))
  @GetMapping(value = "/{semester}/{programme}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  Flux<Course> obtainProgrammeCourses(@PathVariable String semester, @PathVariable String programme,
      @RequestParam(required = false) ClassesLang language,
      @RequestParam(required = false) String code,
      @RequestParam(required = false) Completion completion,
      @RequestParam(required = false) Byte credits,
      @RequestParam(required = false) String department,
      @RequestParam(required = false) String name,
      @RequestParam(required = false) ProgrammeType type,
      @RequestParam(required = false) Season season, @RequestParam(required = false) StudyForm form,
      @RequestParam(required = false, defaultValue = Constants.DEFAULT_API) ApiBase apiBase) {

    return courseService
        .downloadProgrammeCourses(semester, programme, language, code, completion, credits,
            department, name, type, season, form, apiBase);
  }

  @ApiOperation(value = "Get a list with all courses", response = Course.class, responseContainer = "List")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "semester", value = "Semester in which the courses are taught", required = true),
      @ApiImplicitParam(name = "language", value = "Language of the courses", allowEmptyValue = true),
      @ApiImplicitParam(name = "code", value = "Code of the courses as in KOS", allowEmptyValue = true),
      @ApiImplicitParam(name = "completion", value = "Type of completion", allowEmptyValue = true),
      @ApiImplicitParam(name = "credits", value = "Amount of credits", dataType = "Byte", dataTypeClass = Byte.class, allowEmptyValue = true),
      @ApiImplicitParam(name = "department", value = "Department that teaches the courses", allowEmptyValue = true),
      @ApiImplicitParam(name = "name", value = "Name of the courses", allowEmptyValue = true),
      @ApiImplicitParam(name = "type", value = "Programme type the courses are focused for", allowEmptyValue = true),
      @ApiImplicitParam(name = "season", value = "Type  of semester", allowEmptyValue = true),
      @ApiImplicitParam(name = "form", value = "Study form  the courses are focused for", allowEmptyValue = true),
      @ApiImplicitParam(name = "apiBase", value = "KOS API location", defaultValue = Constants.DEFAULT_API)
  })
  @ApiResponses(@ApiResponse(code = 200, message = "Successfully retrieved list of all courses"))
  @GetMapping(value = "/{semester}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  Flux<Course> obtainCourses(@PathVariable String semester,
      @RequestParam(required = false) ClassesLang language,
      @RequestParam(required = false) String code,
      @RequestParam(required = false) Completion completion,
      @RequestParam(required = false) Byte credits,
      @RequestParam(required = false) String department,
      @RequestParam(required = false) String name,
      @RequestParam(required = false) ProgrammeType type,
      @RequestParam(required = false) Season season, @RequestParam(required = false) StudyForm form,
      @RequestParam(required = false, defaultValue = Constants.DEFAULT_API) ApiBase apiBase) {

    return courseService
        .downloadCourses(semester, language, code, completion, credits, department, name, type,
            season, form, apiBase);
  }
}