package impl.course;


import impl.programme.ProgrammeType;
import impl.shared.ClassesLang;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
@ApiModel(description = "Course without parallels")
public class Course implements Serializable {

  private static final long serialVersionUID = 6262182418933072798L;

  @ApiModelProperty("Language of the course")
  private ClassesLang classesLang;
  @ApiModelProperty(value = "Unique code of the course as in KOS", required = true)
  private String code;
  @ApiModelProperty(value = "Action needed to complete the course", required = true)
  private Completion completion;
  @ApiModelProperty(value = "Amount of credits", required = true)
  private short credits;
  @ApiModelProperty(value = "Department that teaches the course", required = true)
  private String department;
  @ApiModelProperty(value = "Name of the course", required = true)
  private String name;
  @ApiModelProperty("Type of the programme the course is focused for")
  private ProgrammeType programmeType;
  @ApiModelProperty("Type of semester in which the course is taught")
  private Season season;
  @ApiModelProperty("Form of the study the course is focused for")
  private StudyForm studyForm;

  public ClassesLang getClassesLang() {
    return classesLang;
  }

  public String getCode() {
    return code;
  }

  public Completion getCompletion() {
    return completion;
  }

  public short getCredits() {
    return credits;
  }

  public String getDepartment() {
    return department;
  }

  public String getName() {
    return name;
  }

  public ProgrammeType getProgrammeType() {
    return programmeType;
  }

  public Season getSeason() {
    return season;
  }

  public StudyForm getStudyForm() {
    return studyForm;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Course)) {
      return false;
    }
    Course course = (Course) o;
    return credits == course.credits &&
        classesLang == course.classesLang &&
        Objects.equals(code, course.code) &&
        completion == course.completion &&
        Objects.equals(department, course.department) &&
        Objects.equals(name, course.name) &&
        programmeType == course.programmeType &&
        season == course.season &&
        studyForm == course.studyForm;
  }

  @Override
  public int hashCode() {
    return Objects.hash(classesLang, code, completion, credits, department, name, programmeType,
        season, studyForm);
  }
}
