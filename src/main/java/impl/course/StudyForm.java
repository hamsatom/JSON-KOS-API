package impl.course;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
enum StudyForm {
  FULLTIME, PARTTIME, DISTANCE, SELF_PAYER, UNDEFINED, Z
}
