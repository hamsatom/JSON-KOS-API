package impl.course;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
enum Completion {
  CLFD_CREDIT, CREDIT, CREDIT_EXAM, DEFENCE, EXAM, NOTHING, UNDEFINED
}
