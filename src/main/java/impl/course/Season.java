package impl.course;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
enum Season {
  WINTER, SUMMER, BOTH, UNDEFINED
}
