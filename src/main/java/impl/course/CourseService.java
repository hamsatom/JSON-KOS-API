package impl.course;

import impl.kos.ApiBase;
import impl.programme.ProgrammeType;
import impl.shared.ClassesLang;
import javax.annotation.Nonnull;
import reactor.core.publisher.Flux;

/**
 * @author Tomáš Hamsa on 18.11.2017.
 */
interface CourseService {

  Flux<Course> downloadProgrammeCourses(@Nonnull String semester, @Nonnull String programmeCode,
      ClassesLang language, String code, Completion completion, Byte credits, String department,
      String name, ProgrammeType type, Season season, StudyForm form, @Nonnull ApiBase apiBase);

  Flux<Course> downloadCourses(@Nonnull String semester, ClassesLang language, String code,
      Completion completion, Byte credits, String department, String name, ProgrammeType type,
      Season season, StudyForm form, @Nonnull ApiBase apiBase);
}
