package config;

import java.lang.reflect.Field;
import java.nio.charset.Charset;
import javax.annotation.Nonnull;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * This class is called when our project is deployed into an application server - the servers have
 * hooks for such cases.
 * <p>
 * It initializes Spring context and starts building beans according to our configuration
 */
public class DispatcherServletInitializer extends
    AbstractAnnotationConfigDispatcherServletInitializer {

  private static void hackSystemEncoding() {
    try {
      System.setProperty("file.encoding", "UTF-8");
      Field charset = Charset.class.getDeclaredField("defaultCharset");
      charset.setAccessible(true);
      charset.set(null, null);
    } catch (Throwable e) {
      e.printStackTrace();
    }
  }

  /**
   * Notice how we are referencing the {@link AppConfig} class, which is basically the root of our
   * application's configuration.
   */
  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class<?>[]{AppConfig.class};
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return null;
  }

  /**
   * This specifies URL paths where the Spring dispatcher servlet will be listening.
   */
  @Nonnull
  @Override
  protected String[] getServletMappings() {
    return new String[]{"/*"};
  }

  @Override
  public void onStartup(@Nonnull ServletContext servletContext) throws ServletException {
    hackSystemEncoding();
    servletContext.addListener(new RequestContextListener());
    super.onStartup(servletContext);
  }
}
