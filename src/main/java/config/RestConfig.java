package config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "impl")
public class RestConfig {

  /**
   * Object mapper is used to serialize POJOs into JSON and vice versa. It is a class from the
   * Jackson framework.
   * <p>
   * We are creating it here so that we can configure it a little.
   */
  @Bean
  public ObjectMapper objectMapper() {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());
    // Only non-null properties are serialized
    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    // This is necessary for our way of working with Java 8 Date/Time API. If it were not configured, the
    // datetime object in JSON would consist of several attributes, each for the respective date/time part, i.e. year, day etc.
    objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    // Ignore unknown properties in JSON
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    return objectMapper;
  }
}
