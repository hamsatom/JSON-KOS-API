package config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Tomáš Hamsa on 22.06.2018.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

  private static ApiInfo metaData() {
    return new ApiInfoBuilder()
        .title("JSON KOS API")
        .description("\"HTTP REST API for KOS using JSON\"")
        .version("1.2.0")
        .contact(new Contact("Tomáš Hamsa", "https://www.facebook.com/tomas.hamsa",
            "okularek@centrum.cz"))
        .build();
  }

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.any())
        .build()
        .apiInfo(metaData());
  }
}
