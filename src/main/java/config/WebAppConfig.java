package config;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.nio.charset.StandardCharsets;
import java.util.List;
import javax.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * This class configures the web services of our application, it tells spring where to find static
 * resources like JS files, so that it can optimize access to them (e.g. skip authentication) and
 * how to handle conversion of JSON to Java and vice versa.
 * <p>
 * This setup is usually done once and then copied into new projects.
 */
@Configuration
@EnableWebMvc
@Import(RestConfig.class)
public class WebAppConfig implements WebMvcConfigurer {

  private final ObjectMapper objectMapper;

  @Autowired
  public WebAppConfig(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @Bean(name = "multipartResolver")
  public MultipartResolver multipartResolver() {
    return new StandardServletMultipartResolver();
  }

  @Override
  public void configureDefaultServletHandling(@Nonnull DefaultServletHandlerConfigurer configurer) {
    configurer.enable();
  }

  @Override
  public void addCorsMappings(@Nonnull CorsRegistry registry) {
    registry.addMapping("/**");
  }

  @Override
  public void addResourceHandlers(@Nonnull ResourceHandlerRegistry registry) {
    registry.addResourceHandler("swagger-ui.html")
        .addResourceLocations("classpath:/META-INF/resources/");

    registry.addResourceHandler("/webjars/**")
        .addResourceLocations("classpath:/META-INF/resources/webjars/");
  }

  @Override
  public void configureMessageConverters(@Nonnull List<HttpMessageConverter<?>> converters) {
    // Here we register Jackson as converter of HTTP messages (in our case, JSON)
    MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    converter.setObjectMapper(objectMapper);
    converters.add(converter);
    // String converter makes sure that e.g. Czech characters are correctly interpreted
    StringHttpMessageConverter stringConverter = new StringHttpMessageConverter(
        StandardCharsets.UTF_8);
    converters.add(stringConverter);
  }
}
