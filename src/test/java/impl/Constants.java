package impl;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
public final class Constants {

  public static final String PROGRAMME = "BP4";
  public static final String SUBJECT_CODE = "A0B04KA";
  public static final String SUBJECT_NAME = "Anglická konverzace";

  private Constants() throws IllegalAccessException {
    throw new IllegalAccessException("Single instance protection");
  }
}
