package impl.kos;

import impl.Constants;
import java.io.IOException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class KosTest {

  @Test
  public void testGetCourseParallels() throws IOException {
    Boolean notEmpty = Kos
        .getCourseParallels(ApiBase.FEL, Kos.getCurrentSemesterCode(), Constants.SUBJECT_CODE, null)
        .hasElements().block();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testGetAllProgrammes() throws IOException {
    Boolean notEmpty = Kos.getAllProgrammes(ApiBase.FIT, Kos.getCurrentSemesterCode(), null)
        .hasElements().block();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testGetProgrammeCourses() throws IOException {
    Boolean notEmpty = Kos
        .getProgrammeCourses(ApiBase.FEL, Kos.getCurrentSemesterCode(), null, Constants.PROGRAMME)
        .hasElements().block();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testGetAllCourses() throws IOException {
    Boolean notEmpty = Kos.getAllCourses(ApiBase.FIT, Kos.getCurrentSemesterCode(), null)
        .hasElements().block();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testGetCourseName() throws IOException {
    String courseName = Kos
        .getCourseName(ApiBase.FEL, Kos.getCurrentSemesterCode(), Constants.SUBJECT_CODE);
    Assert.assertEquals(courseName, Constants.SUBJECT_NAME);
  }
}