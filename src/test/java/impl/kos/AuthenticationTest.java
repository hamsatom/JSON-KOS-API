package impl.kos;

import java.io.IOException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AuthenticationTest {

  @Test
  public void testGetToken() throws IOException {
    String originalToken = Authentication.getToken();
    Assert.assertEquals(Authentication.getToken(), originalToken);
  }
}