package impl.course;

import impl.Constants;
import impl.kos.ApiBase;
import impl.kos.Kos;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reactor.core.publisher.Flux;

public class CourseServiceImplTest extends AbstractCourseTest {

  private CourseService service;

  @BeforeMethod
  public void setUp() {
    service = new CourseServiceImpl();
  }

  @Test
  public void testDownloadProgrammeCoursesNulls() {
    Boolean notEmpty = service
        .downloadProgrammeCourses(Kos.getCurrentSemesterCode(), Constants.PROGRAMME, null, null,
            null, null,
            null, null, null, null, null, ApiBase.FIT).hasElements().block();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testDownloadProgrammeCoursesParams() {
    Flux<Course> courses = service
        .downloadProgrammeCourses(Kos.getCurrentSemesterCode(), Constants.PROGRAMME, LANG, CODE,
            COMPLETION,
            CREDITS, DEPARTMENT, NAME, TYPE, SEASON, FORM, ApiBase.FEL);
    checkCourses(courses);
  }

  @Test
  public void testDownloadCoursesNulls() {
    Boolean notEmpty = service
        .downloadCourses(Kos.getCurrentSemesterCode(), null, null, null, null, null, null, null,
            null, null, ApiBase.FEL).hasElements().block();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testDownloadCoursesParams() {
    Flux<Course> courses = service
        .downloadCourses(Kos.getCurrentSemesterCode(), LANG, CODE, COMPLETION, CREDITS, DEPARTMENT,
            NAME, TYPE, SEASON, FORM, ApiBase.FIT);
    checkCourses(courses);
  }
}