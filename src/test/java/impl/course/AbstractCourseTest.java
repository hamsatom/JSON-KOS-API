package impl.course;

import impl.programme.ProgrammeType;
import impl.shared.ClassesLang;
import org.testng.Assert;
import reactor.core.publisher.Flux;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
abstract class AbstractCourseTest {

  static final ClassesLang LANG = ClassesLang.CS;
  static final String CODE = "a";
  static final Completion COMPLETION = Completion.CREDIT_EXAM;
  static final byte CREDITS = 6;
  static final String DEPARTMENT = "a";
  static final String NAME = "a";
  static final ProgrammeType TYPE = ProgrammeType.BACHELOR;
  static final Season SEASON = Season.WINTER;
  static final StudyForm FORM = StudyForm.FULLTIME;

  static void checkCourses(Flux<Course> courses) {
    courses.subscribe(course -> {
      Assert.assertNotNull(course);
      Assert.assertEquals(course.getClassesLang(), LANG);
      Assert.assertTrue(course.getCode().toLowerCase().contains(CODE));
      Assert.assertEquals(course.getCompletion(), COMPLETION);
      Assert.assertEquals(course.getCredits(), CREDITS);
      Assert.assertTrue(course.getDepartment().toLowerCase().contains(DEPARTMENT));
      Assert.assertTrue(course.getName().toLowerCase().contains(NAME));
      Assert.assertEquals(course.getProgrammeType(), TYPE);
      Assert.assertEquals(course.getSeason(), SEASON);
      Assert.assertEquals(course.getStudyForm(), FORM);
    });
  }
}
