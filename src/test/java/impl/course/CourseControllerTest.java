package impl.course;

import impl.Constants;
import impl.kos.ApiBase;
import impl.kos.Kos;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reactor.core.publisher.Flux;

public class CourseControllerTest extends AbstractCourseTest {

  private CourseController controller;

  @BeforeMethod
  public void setUp() {
    controller = new CourseController(new CourseServiceImpl());
  }

  @Test
  public void testObtainCoursesNulls() {
    Boolean notEmpty = controller
        .obtainCourses(Kos.getCurrentSemesterCode(), null, null, null, null, null, null, null, null,
            null, ApiBase.FEL).hasElements().block();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testObtainCoursesParams() {
    Flux<Course> courses = controller
        .obtainCourses(Kos.getCurrentSemesterCode(), LANG, CODE, COMPLETION, CREDITS, DEPARTMENT,
            NAME, TYPE, SEASON, FORM, ApiBase.FIT);
    checkCourses(courses);
  }

  @Test
  public void testObtainProgrammeCoursesNulls() {
    Boolean notEmpty = controller
        .obtainProgrammeCourses(Kos.getCurrentSemesterCode(), Constants.PROGRAMME, null, null, null,
            null,
            null, null, null, null, null, ApiBase.FIT).hasElements().block();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testObtainProgrammeCoursesParams() {
    Flux<Course> courses = controller
        .obtainProgrammeCourses(Kos.getCurrentSemesterCode(), Constants.PROGRAMME, LANG, CODE,
            COMPLETION,
            CREDITS, DEPARTMENT, NAME, TYPE, SEASON, FORM, ApiBase.FEL);
    checkCourses(courses);
  }
}