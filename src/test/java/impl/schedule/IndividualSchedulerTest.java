package impl.schedule;

import impl.subject.Subject;
import java.util.Collections;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class IndividualSchedulerTest extends AbstractScheduleTest {

  private IndividualScheduler scheduler;

  @BeforeMethod
  public void setUp() {
    scheduler = new IndividualScheduler(THEORY_LESSON_PRIORITY, PRACTICAL_PRIORITY,
        EARLIEST_OK_HOUR, LATEST_OK_HOUR, BAD_HOUR_PENALTY, DAYS_MULTIPLIER, HOURS_MULTIPLIER,
        false);
  }

  @Test
  public void testFindBestScheduleEmpty() {
    Boolean notEmpty = scheduler.findBestSchedule(Collections.emptyList()).hasElements().block();
    Assert.assertFalse(notEmpty);
  }

  @Test
  public void testFindBestSchedule() {
    Subject subject = createSubject();

    List<IndividualSchedule> schedules = scheduler
        .findBestSchedule(Collections.singletonList(subject)).collectList()
        .block();

    Assert.assertNotNull(schedules);
    Assert.assertEquals(schedules.size(), 1);
    IndividualSchedule schedule = schedules.get(0);
    checkSchedule(schedule);
    checkEntries(schedule.get());
  }
}