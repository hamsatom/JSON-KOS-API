package impl.schedule;

import impl.subject.Subject;
import java.util.Collections;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reactor.core.publisher.Flux;

public class ScheduleServiceImplTest extends AbstractScheduleTest {

  private ScheduleService service;

  @BeforeMethod
  public void setUp() {
    service = new ScheduleServiceImpl();
  }

  @Test
  public void testBuildIndividualSchedulesEmpty() {
    Boolean notEmpty = service
        .buildIndividualSchedules(THEORY_LESSON_PRIORITY, PRACTICAL_PRIORITY, EARLIEST_OK_HOUR,
            LATEST_OK_HOUR, BAD_HOUR_PENALTY, DAYS_MULTIPLIER, HOURS_MULTIPLIER, true,
            Collections.emptyList()).hasElements().block();
    Assert.assertFalse(notEmpty);
  }

  @Test
  public void testBuildIndividualSchedules() {
    Subject subject = createSubject();

    List<ScheduleEntry> schedules = service
        .buildIndividualSchedules(THEORY_LESSON_PRIORITY, PRACTICAL_PRIORITY, EARLIEST_OK_HOUR,
            LATEST_OK_HOUR, BAD_HOUR_PENALTY, DAYS_MULTIPLIER, HOURS_MULTIPLIER, false,
            Collections.singletonList(subject)).flatMap(Flux::fromIterable).collectList().block();

    checkEntries(schedules);
  }
}