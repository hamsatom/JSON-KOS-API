package impl.schedule;

import impl.parallel.Parallel;
import impl.parallel.ParallelType;
import impl.parallel.TimetableSlot;
import impl.shared.Permission;
import impl.subject.Subject;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.testng.Assert;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
abstract class AbstractScheduleTest {

  static final short PRACTICAL_PRIORITY = 2;
  static final short THEORY_LESSON_PRIORITY = 1;
  static final short EARLIEST_OK_HOUR = 3;
  static final short LATEST_OK_HOUR = 12;
  static final double BAD_HOUR_PENALTY = 1.5;
  static final double DAYS_MULTIPLIER = 2.0;
  static final double HOURS_MULTIPLIER = 1.0;
  private static final short DURATION = 2;
  private static final short DAY = 5;
  private static final short START_HOUR = 15;
  private static final List<String> TEACHERS = Collections.singletonList("a");
  private static final String ROOM = "r";
  private static final ParallelType TYPE = ParallelType.TUTORIAL;
  private static final String NAME = "n";
  private static final String CODE = "c";

  static ScheduleEntry createEntry() {
    return new ScheduleEntry(Short.MAX_VALUE, DAY, START_HOUR, DURATION, ROOM,
        TEACHERS, TYPE, NAME, CODE);
  }

  static Subject createSubject() {
    TimetableSlot slot = new TimetableSlot(DAY, DURATION, START_HOUR, ROOM);
    Parallel parallel = new Parallel(TYPE, TEACHERS, Collections.singletonList(slot), (short) 10,
        (short) 9, Permission.DENIED);
    return new Subject(NAME, CODE, Collections.singletonList(parallel));
  }

  static void checkSchedule(IndividualSchedule schedule) {
    Assert.assertNotNull(schedule);
    double countedHours = schedule.countHoursInSchool(Short.MIN_VALUE, Short.MAX_VALUE, 1);
    Assert.assertEquals(countedHours, (double) DURATION);
    Assert.assertEquals(schedule.getDaysWithOnlyTheoryLessonsCount(), 4);
    Assert.assertEquals(schedule.getFreeDaysCount(), 4);
    ScheduleEntry full = new ScheduleEntry(Short.MIN_VALUE, DAY, START_HOUR, DURATION,
        null, null, null, null, null);
    Assert.assertFalse(schedule.isSpaceEmpty(full));

    Collection<ScheduleEntry> entries = schedule.get();
    Assert.assertNotNull(entries);
    Assert.assertEquals(entries.size(), 1);
  }

  static void checkEntries(Collection<? extends ScheduleEntry> entries) {
    Assert.assertNotNull(entries);
    Assert.assertEquals(entries.size(), 1);
    entries.forEach(entry -> {
      Assert.assertNotNull(entry);
      Assert.assertEquals(DAY, entry.getDay());
      Assert.assertEquals(ROOM, entry.getRoom());
      Assert.assertEquals(TEACHERS, entry.getTeacher());
      Assert.assertEquals(START_HOUR, entry.getFirstHour());
      Assert.assertEquals(TYPE, entry.getParallelType());
      Assert.assertEquals(DURATION, entry.getDuration());
      Assert.assertEquals(NAME, entry.getName());
      Assert.assertEquals(CODE, entry.getCode());
      Assert.assertEquals(PRACTICAL_PRIORITY, entry.getPriority());
    });
  }
}
