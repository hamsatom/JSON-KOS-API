package impl.schedule;

import impl.subject.Subject;
import java.util.Collections;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reactor.core.publisher.Flux;

public class ScheduleControllerTest extends AbstractScheduleTest {

  private ScheduleController controller;

  @BeforeMethod
  public void setUp() {
    controller = new ScheduleController(new ScheduleServiceImpl());
  }

  @Test
  public void testCreateIndividualScheduleEmpty() {
    Boolean notEmpty = controller
        .createIndividualSchedule(THEORY_LESSON_PRIORITY, PRACTICAL_PRIORITY, EARLIEST_OK_HOUR,
            LATEST_OK_HOUR, BAD_HOUR_PENALTY, DAYS_MULTIPLIER, HOURS_MULTIPLIER,
            Collections.emptyList()).hasElements().block();
    Assert.assertFalse(notEmpty);
  }

  @Test
  public void testCreateIndividualSchedule() {
    Subject subject = createSubject();

    List<ScheduleEntry> schedules = controller
        .createIndividualSchedule(THEORY_LESSON_PRIORITY, PRACTICAL_PRIORITY, EARLIEST_OK_HOUR,
            LATEST_OK_HOUR, BAD_HOUR_PENALTY, DAYS_MULTIPLIER, HOURS_MULTIPLIER,
            Collections.singletonList(subject)).flatMap(Flux::fromIterable).collectList().block();

    checkEntries(schedules);
  }

  @Test
  public void testCreateIndividualScheduleSeparatedEmpty() {
    Boolean notEmpty = controller
        .createIndividualScheduleSeparated(THEORY_LESSON_PRIORITY, PRACTICAL_PRIORITY,
            EARLIEST_OK_HOUR, LATEST_OK_HOUR, BAD_HOUR_PENALTY, DAYS_MULTIPLIER, HOURS_MULTIPLIER,
            Collections.emptyList()).hasElements().block();
    Assert.assertFalse(notEmpty);
  }

  @Test
  public void testCreateIndividualScheduleSeparated() {
    Subject subject = createSubject();

    List<ScheduleEntry> entries = controller
        .createIndividualScheduleSeparated(THEORY_LESSON_PRIORITY, PRACTICAL_PRIORITY,
            EARLIEST_OK_HOUR, LATEST_OK_HOUR, BAD_HOUR_PENALTY, DAYS_MULTIPLIER, HOURS_MULTIPLIER,
            Collections.singletonList(subject)).flatMap(Flux::fromIterable).collectList().block();

    checkEntries(entries);
  }
}