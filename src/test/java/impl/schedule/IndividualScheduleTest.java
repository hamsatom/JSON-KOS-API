package impl.schedule;

import java.util.Collection;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class IndividualScheduleTest extends AbstractScheduleTest {

  private IndividualSchedule schedule;

  @BeforeMethod
  public void setUp() {
    schedule = new IndividualSchedule();
  }

  @Test
  public void testGetCopy() {
    Assert.assertEquals(schedule, schedule.getCopy());
  }

  @Test
  public void testGetFreeDaysCount() {
    Assert.assertEquals(schedule.getFreeDaysCount(), 5);
  }

  @Test
  public void testGetDaysWithOnlyTheoryLessonsCount() {
    Assert.assertEquals(schedule.getDaysWithOnlyTheoryLessonsCount(), 5);
  }

  @Test
  public void testCountHoursInSchool() {
    double count = schedule.countHoursInSchool(EARLIEST_OK_HOUR, LATEST_OK_HOUR, BAD_HOUR_PENALTY);
    Assert.assertEquals(count, 0.0);
  }

  @Test
  public void testGet() {
    Collection<ScheduleEntry> entries = schedule.get();
    Assert.assertNotNull(entries);
    Assert.assertTrue(entries.isEmpty());
  }

  @Test
  public void testSpaceEmpty() {
    ScheduleEntry entry = createEntry();
    Assert.assertTrue(schedule.isSpaceEmpty(entry));
  }

  @Test
  public void testNotEmpty() {
    ScheduleEntry entry = createEntry();
    schedule.addClass(entry);
    checkSchedule(schedule);
    schedule.get().forEach(actualEntry -> Assert.assertEquals(actualEntry, entry));
  }
}