package impl.notification;

import impl.Constants;
import impl.kos.ApiBase;
import impl.kos.Kos;
import impl.parallel.ParallelType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NotificationControllerTest {

  private NotificationController controller;

  @BeforeMethod
  public void setUp() {
    controller = new NotificationController(new NotificationServiceImpl(new JavaMailSenderImpl()));
  }

  @Test
  public void testSignForNotificationInvalidMail() {
    ResponseEntity<Object> responseEntity = callController((byte) 5, "invalidMail");
    Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.BAD_REQUEST);
    Assert.assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.BAD_REQUEST.value());
  }

  @Test
  public void testSignForNotificationInvalidDay() {
    ResponseEntity<Object> responseEntity = callController((byte) 0, "mail@gmail.com");
    Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.BAD_REQUEST);
    Assert.assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.BAD_REQUEST.value());
  }

  @Test
  public void testSignForNotification() {
    ResponseEntity<Object> responseEntity = callController((byte) 5, "mail@gmail.com");
    Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NO_CONTENT);
    Assert.assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.NO_CONTENT.value());
  }

  private ResponseEntity<Object> callController(byte day, String mail) {
    ResponseEntity<Object> responseEntity = controller
        .signForNotification(Kos.getCurrentSemesterCode(), Constants.SUBJECT_CODE, day, (byte) 1,
            (byte) 17,
            mail, ParallelType.LABORATORY, ApiBase.FIT);
    Assert.assertNotNull(responseEntity);
    Assert.assertNull(responseEntity.getBody());
    Assert.assertTrue(responseEntity.getHeaders().isEmpty());
    return responseEntity;
  }
}