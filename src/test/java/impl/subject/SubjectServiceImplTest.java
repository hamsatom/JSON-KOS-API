package impl.subject;

import impl.Constants;
import impl.kos.ApiBase;
import impl.kos.Kos;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SubjectServiceImplTest extends AbstractSubjectTest {

  private SubjectService service;

  @BeforeMethod
  public void setUp() {
    service = new SubjectServiceImpl();
  }

  @Test
  public void testDownloadOpenNotFullSubjects() {
    Subject subject = service
        .downloadOpenNotFullSubjects(Kos.getCurrentSemesterCode(), Constants.SUBJECT_CODE,
            ApiBase.FEL)
        .block();
    checkNotFull(subject);
  }

  @Test
  public void testDownloadOpenSubjects() {
    Subject subject = service
        .downloadOpenSubjects(Kos.getCurrentSemesterCode(), Constants.SUBJECT_CODE, ApiBase.FIT)
        .block();
    getParallels(subject);
  }

  @Test
  public void testDownloadNotFullSubjects() {
    Subject subject = service
        .downloadNotFullSubjects(Kos.getCurrentSemesterCode(), Constants.SUBJECT_CODE, ApiBase.FEL)
        .block();
    checkNotFull(subject);
  }

  @Test
  public void testDownloadSubjects() {
    Subject subject = service
        .downloadSubjects(Kos.getCurrentSemesterCode(), Constants.SUBJECT_CODE, ApiBase.FIT)
        .block();
    checkNotEmpty(subject);
  }
}