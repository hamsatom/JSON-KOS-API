package impl.subject;

import impl.Constants;
import impl.kos.ApiBase;
import impl.kos.Kos;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SubjectControllerTest extends AbstractSubjectTest {

  private SubjectController controller;

  @BeforeMethod
  public void setUp() {
    controller = new SubjectController(new SubjectServiceImpl());
  }

  @Test
  public void testAllSubjects() {
    Subject subject = controller
        .allSubjects(Kos.getCurrentSemesterCode(), Constants.SUBJECT_CODE, ApiBase.FIT)
        .block();
    checkNotEmpty(subject);
  }

  @Test
  public void testOpenSubjects() {
    Subject subject = controller
        .openSubjects(Kos.getCurrentSemesterCode(), Constants.SUBJECT_CODE, ApiBase.FEL).block();
    getParallels(subject);
  }

  @Test
  public void testNotFullSubjects() {
    Subject subject = controller
        .notFullSubjects(Kos.getCurrentSemesterCode(), Constants.SUBJECT_CODE, ApiBase.FIT).block();
    checkNotFull(subject);
  }

  @Test
  public void testOpenNotFullSubjects() {
    Subject subject = controller
        .openNotFullSubjects(Kos.getCurrentSemesterCode(), Constants.SUBJECT_CODE, ApiBase.FEL)
        .block();
    checkNotFull(subject);
  }
}