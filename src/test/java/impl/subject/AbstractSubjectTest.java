package impl.subject;

import impl.Constants;
import impl.parallel.Parallel;
import impl.shared.Permission;
import java.util.Collection;
import org.testng.Assert;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
abstract class AbstractSubjectTest {

  static Collection<Parallel> getParallels(Subject subject) {
    Assert.assertNotNull(subject);
    Assert.assertEquals(subject.getCode(), Constants.SUBJECT_CODE);
    Assert.assertEquals(subject.getName(), Constants.SUBJECT_NAME);
    Collection<Parallel> parallels = subject.getParallels();
    Assert.assertNotNull(parallels);
    return parallels;
  }

  static void checkNotEmpty(Subject subject) {
    Collection<Parallel> parallels = getParallels(subject);
    Assert.assertFalse(parallels.isEmpty());
  }

  static void checkNotFull(Subject subject) {
    getParallels(subject).forEach(parallel -> Assert.assertTrue(
        parallel.getCapacity() > parallel.getOccupied()
            || parallel.getCapacityOverfill() == Permission.ALLOWED));
  }
}
