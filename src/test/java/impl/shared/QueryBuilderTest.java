package impl.shared;

import impl.parallel.ParallelType;
import org.testng.Assert;
import org.testng.annotations.Test;

public class QueryBuilderTest {

  @Test
  public void testBuild() {
    Assert.assertEquals(QueryBuilder.buildQuery(), "");
  }

  @Test
  public void testAddObject() {
    String query = QueryBuilder.buildQuery("a", 1, "b", 2);
    Assert.assertEquals(query, "a==1;b==2");
  }

  @Test
  public void testAddEnum() {
    ParallelType tutorial = ParallelType.TUTORIAL;
    ParallelType laboratory = ParallelType.LABORATORY;
    String query = QueryBuilder.buildQuery("a", tutorial, "b", laboratory);
    Assert.assertEquals(query, "a==" + tutorial.name() + ";b==" + laboratory.name());
  }

  @Test
  public void testAddString() {
    String query = QueryBuilder.buildQuery("a", "a", "b", "b");
    Assert.assertEquals(query, "a=='*a*';b=='*b*'");
  }

  @Test
  public void testAddNull() {
    String query = QueryBuilder.buildQuery("a", null, "b", "b");
    Assert.assertEquals(query, "b=='*b*'");
  }

  @Test(priority = 1)
  public void testAddAll() {
    String expected = "string=='*stringValue*';enum==" + ParallelType.TUTORIAL.name() + ";short==5";
    String actual = QueryBuilder
        .buildQuery("string", "stringValue", "enum", ParallelType.TUTORIAL, "short", (short) 5,
            "null", null);
    Assert.assertEquals(actual, expected);

  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testWringParams() {
    QueryBuilder.buildQuery("a", "a", "b");
  }
}