package impl.programme;

import impl.kos.ApiBase;
import impl.kos.Kos;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reactor.core.publisher.Flux;

public class ProgrammeControllerTest extends AbstractProgrammeTest {

  private ProgrammeController controller;

  @BeforeMethod
  public void setUp() {
    controller = new ProgrammeController(new ProgrammeServiceImpl());
  }

  @Test
  public void testObtainProgrammesNull() {
    Boolean notEmpty = controller
        .obtainProgrammes(Kos.getCurrentSemesterCode(), null, null, null, null, null, null, null,
            ApiBase.FEL).hasElements().block();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testObtainProgrammesParams() {
    Flux<Programme> programmes = controller
        .obtainProgrammes(Kos.getCurrentSemesterCode(), LANG, CODE, DURATION, FACULTY, NAME, TYPE,
            OPEN, ApiBase.FIT);
    checkProgrammes(programmes);
  }
}