package impl.programme;

import impl.shared.ClassesLang;
import org.testng.Assert;
import reactor.core.publisher.Flux;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
abstract class AbstractProgrammeTest {

  static final ClassesLang LANG = ClassesLang.CS;
  static final String CODE = "a";
  static final float DURATION = 3.0f;
  static final String FACULTY = "a";
  static final String NAME = "a";
  static final ProgrammeType TYPE = ProgrammeType.BACHELOR;
  static final boolean OPEN = true;

  static void checkProgrammes(Flux<Programme> programmes) {
    programmes.subscribe(programme -> {
      Assert.assertNotNull(programme);
      Assert.assertEquals(programme.getClassesLang(), LANG);
      Assert.assertTrue(programme.getCode().toLowerCase().contains(CODE));
      Assert.assertEquals(programme.getStudyDuration(), DURATION);
      Assert.assertTrue(programme.getFaculty().toLowerCase().contains(FACULTY));
      Assert.assertTrue(programme.getDiplomaName().toLowerCase().contains(NAME));
      Assert.assertEquals(programme.getType(), TYPE);
      Assert.assertEquals(programme.isOpenForAdmission(), OPEN);
    });
  }
}
