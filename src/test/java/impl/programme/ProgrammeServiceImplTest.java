package impl.programme;

import impl.kos.ApiBase;
import impl.kos.Kos;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reactor.core.publisher.Flux;

public class ProgrammeServiceImplTest extends AbstractProgrammeTest {

  private ProgrammeService service;

  @BeforeMethod
  public void setUp() {
    service = new ProgrammeServiceImpl();
  }

  @Test
  public void testDownloadProgrammesNulls() {
    Boolean notEmpty = service
        .downloadProgrammes(Kos.getCurrentSemesterCode(), null, null, null, null, null, null, null,
            ApiBase.FEL).hasElements().block();
    Assert.assertTrue(notEmpty);
  }

  @Test
  public void testDownloadProgrammesParams() {
    Flux<Programme> programmes = service
        .downloadProgrammes(Kos.getCurrentSemesterCode(), LANG, CODE, FACULTY, NAME, TYPE, OPEN,
            DURATION, ApiBase.FIT);
    checkProgrammes(programmes);
  }
}